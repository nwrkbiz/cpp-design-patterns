#!/bin/bash

set -x
set -e

./scripts/commitlint.sh
./scripts/dockerlint.sh ./.devcontainer
./scripts/shellcheck.sh ./scripts
./scripts/spellcheck.sh .
./scripts/cppcheck.sh ./docs
./scripts/cppcodingstyle.sh .
./scripts/unit_tests.sh
./scripts/memory_check.sh
./scripts/build_linux.sh RelWithDebInfo
./scripts/doxygen.sh

echo "All is O.k!"