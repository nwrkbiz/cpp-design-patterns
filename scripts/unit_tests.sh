#!/bin/sh

set -x

mkdir -p utest
cd ./utest || exit 1
J=$(nproc)
cmake .. -DENABLE_TESTS=On -DCMAKE_BUILD_TYPE=Debug
cmake --build . --parallel "$J"
OUTPUT=$(ctest --parallel "$J" --output-junit test_report.xml 2>&1)
EC=$?
if [ $EC -ne 0 ]; then
    cd - || exit 1
    echo "$OUTPUT" > testerrors.log
    exit $EC
fi
cd - || exit 1

SUBMODULES=$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }')
IGNORE_PARAM=$(printf '%s\n' "$SUBMODULES" | sed 's/[^[:space:]]\{1,\}/-e ..\/..\/&/g')

mkdir -p ./utest/coverage_report
cd ./utest/coverage_report || exit 1

# code coverage barrier
J=$(nproc)
# shellcheck disable=SC2086
OUTPUT=$(gcovr -j "$J" --sonarqube sonarqube_coverage_report.xml --cobertura coverage_report.xml --html-details coverage_report.html --print-summary -f '../../include' $IGNORE_PARAM --fail-under-line 100 -r "../../" 2>&1)
EC=$?
if [ $EC -ne 0 ]; then
    cd - || exit 1
    echo "$OUTPUT" > testerrors.log
    exit $EC
fi

cd - || exit 1
