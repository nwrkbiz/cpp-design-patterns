#!/bin/sh

set -x

mkdir -p build_doc
cd ./build_doc || exit 1
cmake ..
OUTPUT=$(doxygen 2>&1)
EC=$?
if [ $EC -ne 0 ]; then
    cd - || :
    echo "$OUTPUT" > doxygenerrors.log
    exit $EC
fi
if [ ! -e "./documentation/index.html" ]; then
    echo "Failed to generate documentation!"
    exit 1
fi
