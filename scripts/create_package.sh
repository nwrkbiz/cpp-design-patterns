#!/bin/sh

set -x

if [ "$#" -ne "1" ]; then
    echo "Usage: $0 <release-type>"
    exit 1
fi

mkdir -p pack_linux
cd ./pack_linux || exit 1
cmake .. -DENABLE_TESTS=Off -DENABLE_CLANG_TIDY=Off -DCMAKE_BUILD_TYPE="$1"
J="$(nproc)"
cmake --build . --parallel "$J"
OUTPUT=$(cpack -G DEB 2>&1)
EC=$?
cd - || exit 1
if [ $EC -ne 0 ]; then
    echo "$OUTPUT" > pack_errors.log
    exit $EC
fi

