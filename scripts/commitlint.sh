#!/bin/sh

set -x

# add develop as origin (needed in pipelines that only checkout the feat branch)
git remote set-branches --add origin develop
git fetch origin develop

COMMITS=$(git rev-list --count HEAD ^origin/develop)
COMMITS=${COMMITS:-1}
if [ "$COMMITS" -eq 0 ]; then
    COMMITS=1
fi
OUTPUT=$(commitlint -V --extends @commitlint/config-conventional --from=HEAD~"$COMMITS" --to HEAD 2>&1)

EC=$?
if [ $EC -ne 0 ]; then
    echo "$OUTPUT" > commitmsgerrors.log
    exit $EC
fi

# check for spelling errors
MESSAGES=$(git log -"$COMMITS" --pretty=%B)
OUTPUT=$(echo "$MESSAGES" | codespell -)
EC=$?
if [ $EC -ne 0 ]; then
    echo "$OUTPUT" > commitspellingerrors.log
    exit $EC
fi