#!/bin/sh

set -x

mkdir -p memcheck
cd ./memcheck || exit 1
cmake .. -DENABLE_TESTS=On -DENABLE_MEMCHECK=On -DCMAKE_BUILD_TYPE=Debug
cmake --build . --parallel "$(nproc)"
J=$(nproc)
OUTPUT=$(ctest -j "$J" --overwrite MemoryCheckCommandOptions="--leak-check=full --show-leak-kinds=all --error-exitcode=1" --output-on-failure -T memcheck 2>&1)
EC=$?
if [ $EC -ne 0 ]; then
    cd - || exit 1
    echo "$OUTPUT" > memcheckrrors.log
    exit $EC
fi
cd - || exit 1
