#!/bin/sh

set -x

if [ "$#" -ne "1" ]; then
	echo "Usage: $0 <src-folder>"
    exit 1
fi

SRC="$1"
FILES=$(find "$SRC" -type f -iname "Dockerfile*" -o -iname "dockerfile*")

# We want the paths to split here
# shellcheck disable=SC2086
OUTPUT=$(hadolint --ignore "DL3016" --ignore "DL3008" $FILES 2>&1)

EC=$?
if [ $EC -ne 0 ]; then
    echo "$OUTPUT" > dockerfileerrors.log
    exit $EC
fi
