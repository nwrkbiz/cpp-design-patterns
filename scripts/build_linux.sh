#!/bin/sh

set -x

if [ "$#" -ne "1" ]; then
    echo "Usage: $0 <release-type>"
    exit 1
fi

mkdir -p build_linux
cd ./build_linux || exit 1
cmake .. -DENABLE_TESTS=Off -DENABLE_EXAMPLES=On -DENABLE_CLANG_TIDY=On -DCMAKE_BUILD_TYPE="$1"
J="$(nproc)"
OUTPUT=$(cmake --build . --parallel "$J" 2>&1)
EC=$?
cd - || exit 1
if [ $EC -ne 0 ]; then
    echo "$OUTPUT" > build_linuxerrors.log
    exit $EC
fi

