#!/bin/sh

set -x

if [ "$#" -ne "1" ]; then
    echo "Usage: $0 <release-type>"
    exit 1
fi

mkdir -p build_windows
cd ./build_windows || exit 1
cmake .. -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/x86_64-w64-mingw32-g++ -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/x86_64-w64-mingw32-gcc -DENABLE_TESTS=Off -DENABLE_EXAMPLES=On -DENABLE_CLANG_TIDY=On -DCMAKE_BUILD_TYPE="$1"
J="$(nproc)"
OUTPUT=$(cmake --build . --parallel "$J" 2>&1)
EC=$?
cd - || exit 1
if [ $EC -ne 0 ]; then
    echo "$OUTPUT" > build_windowserrors.log
    exit $EC
fi

