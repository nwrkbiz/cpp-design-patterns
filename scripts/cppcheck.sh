#!/bin/sh

set -x

if [ "$#" -ne "1" ]; then
    echo "Usage: $0 <src-folder>"
    exit 1
fi

SRC="$1"

SUBMODULES=$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }')
IGNORE_PARAM=$(printf '%s\n' "$SUBMODULES" | sed 's/[^[:space:]]\{1,\}/-i&/g')
# shellcheck disable=SC2086
OUTPUT=$(cppcheck --error-exitcode=2 --suppress=ctuOneDefinitionRuleViolation "$PWD"/src -I"$PWD"/include "$SRC" -I"$SRC" -itests $IGNORE_PARAM 2>&1)

EC=$?
if [ $EC -ne 0 ]; then
    echo "$OUTPUT" > codingerrors.log
    exit $EC
fi
