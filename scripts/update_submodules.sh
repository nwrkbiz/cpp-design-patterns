#!/bin/bash

SUBMODULES=$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }')

SAVEIFS=$IFS
# shellcheck disable=SC3003
IFS=$'\n'
# shellcheck disable=SC2206,SC3030
SUBMODULES=($SUBMODULES)
IFS=$SAVEIFS


# shellcheck disable=SC3005,SC3018
for (( i=0; i<${#SUBMODULES[@]}; i++ ))
do
    # shellcheck disable=SC3054
    cd "${SUBMODULES[$i]}" || exit
    git checkout develop
    git pull
    git submodule update --recursive
    cd - || exit
    # shellcheck disable=SC3054
    git add "${SUBMODULES[$i]}"
done
