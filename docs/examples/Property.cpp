/**
 * @file Property.cpp
 * @brief Example showing how to utilize the property template classes.
 * @author Daniel Giritzer, MSc
 */
// Property example.
#include <Dg/Patterns/Property.h>

#include <iostream>
#include <string>

// class to showcase usage with complex types
class Greeter : public Dg::Object<Greeter> {
    std::string m_name;

public:
    Greeter(std::string name)
        : m_name(std::move(name))
    {
    }

    auto greet() -> void
    {
        std::cout << "Hello, " + m_name << std::endl;
    }
};

// class to showcase the usage of the auto properties
class ExampleClassAutoProperty : public Dg::Object<ExampleClassAutoProperty> {
public:
    Dg::Patterns::Property::Auto<std::string> greetingProperty { "Hello" };
    Dg::Patterns::Property::Auto<std::string>::ReadOnly greetingPropertyReadOnly { "Hello" };

    Dg::Patterns::Property::Auto<size_t> getTwiceAsMuch {
        0, // initial value
        [](size_t const& value) -> size_t { return value * 2; } // getter
    };

    Dg::Patterns::Property::Auto<size_t> setTheHalf {
        0, // initial value
        [](size_t const& value) -> size_t { return value; }, // getter
        [](size_t& value, size_t const& newValue) -> void { value = newValue / 2; } // setter
    };

    Dg::Patterns::Property::Auto<Greeter::SPtr> complexObject { std::make_shared<Greeter>("Nino") };
};

// class to showcase the usage of the manual properties
class ExampleClassManualProperty : public Dg::Object<ExampleClassManualProperty> {
    size_t m_field = 0;

public:
    Dg::Patterns::Property::Manual<size_t> fieldProperty {
        [&]() -> size_t& { return m_field; }, // getter
        [&](size_t const& value) { m_field = value; } // setter
    };

    Dg::Patterns::Property::Manual<size_t>::ReadOnly fieldPropertyReadonly {
        [&]() -> size_t& { return m_field; } // getter
    };
};

auto main() -> int
{
    std::cout << "AutoProperty examples: " << std::endl;
    auto exampleObj = std::make_shared<ExampleClassAutoProperty>();

    exampleObj->greetingProperty = std::string(exampleObj->greetingProperty) + ", World!";
    std::cout << exampleObj->greetingProperty << std::endl;

    // exampleObj->greetingPropertyReadOnly = std::string(exampleObj->greetingPropertyReadOnly) + ", World!"; // not possible, compile error
    std::cout << exampleObj->greetingPropertyReadOnly << std::endl;

    exampleObj->getTwiceAsMuch = 3;
    std::cout << exampleObj->getTwiceAsMuch << std::endl;

    exampleObj->setTheHalf = 4;
    std::cout << exampleObj->setTheHalf << std::endl;

    exampleObj->setTheHalf = exampleObj->setTheHalf + 4;
    std::cout << exampleObj->setTheHalf << std::endl;

    exampleObj->complexObject->greet();
    exampleObj->complexObject = std::make_shared<Greeter>("Mimi");
    exampleObj->complexObject->greet();

    std::cout << std::endl
              << "ManualProperty examples: " << std::endl;
    auto exampleObj2 = std::make_shared<ExampleClassManualProperty>();
    exampleObj2->fieldProperty = 3;
    std::cout << exampleObj2->fieldProperty << std::endl;

    // exampleObj2->fieldPropertyReadonly = 3; not possible, compile error
    std::cout << exampleObj2->fieldPropertyReadonly << std::endl;

    return EXIT_SUCCESS;
}

// Output:
// AutoProperty examples:
// Hello, World!
// Hello
// 6
// 2
// 3
// Hello, Nino
// Hello, Mimi
//
// ManualProperty examples:
// 3
// 3
