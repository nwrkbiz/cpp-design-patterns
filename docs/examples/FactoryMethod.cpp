/**
 * @file FactoryMethod.cpp
 * @brief Example showing how to utilize the factory method template classes.
 * @author Daniel Giritzer, MSc
 */
// FactoryMethod example.
#include <Dg/Patterns/FactoryMethod.h>

#include <iostream>
#include <string>

// define an interface for the concrete object to create
class IGreeter : Dg::Object<IGreeter> {
public:
    virtual auto greet() -> void = 0;
};

// concrete object
class SayHi : public IGreeter {
    std::string m_name;

public:
    // convenience using statements for the factory method type
    using Factory = Dg::Patterns::FactoryMethod<IGreeter, SayHi, const std::string&>;
    using IFactory = Dg::Patterns::IFactoryMethod<IGreeter, const std::string&>;
    SayHi(std::string name)
        : m_name(std::move(name))
    {
    }
    auto greet() -> void override
    {
        std::cout << "Hi, " << m_name << std::endl;
    }
};

auto main() -> int
{
    // create a factory which is able to create concrete objects of our interface
    auto myFactory = std::make_shared<SayHi::Factory>();

    // use the interface
    SayHi::IFactory::SPtr factoryIF = myFactory;
    factoryIF->createShared("Nino")->greet();
    factoryIF->createUnique("Mimi")->greet();
    return EXIT_SUCCESS;
}

// Output:
// Hi, Nino
// Hi, Mimi