/**
 * @file Memento.cpp
 * @brief Example showing how to utilize the memento template class.
 * @author Daniel Giritzer, MSc
 */
// Memento example.
#include <Dg/Patterns/Memento.h>

#include <iostream>
#include <string>

// editor class utilizing the memento pattern to store state snapshots
class Editor : public Dg::Patterns::IOriginator<Editor> {
    std::string m_text;
    size_t m_cursor = 0;

    // internal, hidden memento to store the objects data/state
    class EditorData : public Dg::Patterns::Memento<Editor> {
        friend Editor;
        std::string m_text;
        size_t m_cursor = 0;

    public:
        auto restore() -> void override
        {
            getOriginator()->m_text = m_text;
            getOriginator()->m_cursor = m_cursor;
        };
    };

public:
    // function to create the memento/snapshot
    auto createMemento() -> Dg::Patterns::IMemento<Editor>::SPtr override
    {
        auto snapshot = std::make_shared<EditorData>();
        snapshot->setOriginator(shared_from_this());
        snapshot->m_cursor = m_cursor;
        snapshot->m_text = m_text;
        return snapshot;
    }

    auto writeText(const std::string& chars) -> void
    {
        m_text.insert(m_cursor, chars);
        m_cursor += chars.size();
    }

    auto setCursor(size_t pos) -> void
    {
        if (pos >= 0 && pos <= m_text.size()) {
            m_cursor = pos;
        }
    }

    auto printStatus() -> void
    {
        std::string toPrint = m_text;
        toPrint.insert(m_cursor, "|");
        std::cout << toPrint << std::endl;
    };
};

auto main() -> int
{
    auto editor = std::make_shared<Editor>();
    editor->writeText("Hello, ");

    auto snapshot = editor->createMemento();
    editor->writeText("world!");

    constexpr size_t CURSOR_OFFSET = 5;
    editor->setCursor(CURSOR_OFFSET);
    editor->printStatus();

    std::cout << std::endl
              << "Restored..." << std::endl;
    snapshot->restore();
    editor->printStatus();

    return EXIT_SUCCESS;
}

// Output:
// Hello|, world!
//
// Restored...
// Hello, |