/**
 * @file SimpleFactory.cpp
 * @brief Example showing how to utilize the simple factory template classes.
 * @author Daniel Giritzer, MSc
 */
// SimpleFactory example.
#include <Dg/Patterns/SimpleFactory.h>

#include <iostream>
#include <string>

// define an interface for the concrete objects to create
class IGreeter : Dg::Object<IGreeter> {
public:
    virtual auto greetSomeHow() -> void = 0;
};

// first concrete object
class SayHi : public IGreeter {
    std::string m_name;

public:
    SayHi(std::string name)
        : m_name(std::move(name))
    {
    }
    auto greetSomeHow() -> void override
    {
        std::cout << "Hi, " << m_name << std::endl;
    }
};

// second concrete object
class SayHello : public IGreeter {
    std::string m_name;

public:
    SayHello(std::string name)
        : m_name(std::move(name))
    {
    }
    auto greetSomeHow() -> void override
    {
        std::cout << "Hello, " << m_name << std::endl;
    }
};

auto main() -> int
{
    // create a factory which is able to create concrete objects of our interface
    auto myFactory = std::make_shared<Dg::Patterns::SimpleFactory<std::string, IGreeter, const std::string&>>();

    // creator functions for our first concrete object
    Dg::Patterns::SimpleFactory<std::string, IGreeter, const std::string&>::creatorFunctions creatorsHi {
        [](const std::string& name) {
            return std::make_shared<SayHi>(name);
        },
        [](const std::string& name) {
            return std::make_unique<SayHi>(name);
        },
        [](const std::string& name) {
            return new SayHi(name); // NOLINT
        }
    };

    // creator functions for our second concrete object
    Dg::Patterns::SimpleFactory<std::string, IGreeter, const std::string&>::creatorFunctions creatorsHello {
        [](const std::string& name) {
            return std::make_shared<SayHello>(name);
        },
        [](const std::string& name) {
            return std::make_unique<SayHello>(name);
        },
        [](const std::string& name) {
            return new SayHello(name); // NOLINT
        }
    };

    // register our creator functions
    myFactory->registerCreator("hi", creatorsHi);
    myFactory->registerCreator("hello", creatorsHello);

    // test the object creation
    myFactory->createUnique("hi", "Nino")->greetSomeHow();
    myFactory->createShared("hello", "Mimi")->greetSomeHow();

    return EXIT_SUCCESS;
}

// Output:
// Hi, Nino
// Hello, Mimi