/**
 * @file Prototype.cpp
 * @brief Example showing how to utilize the prototype template classes.
 * @author Daniel Giritzer, MSc
 */
// Prototype example.
#include <Dg/Patterns/Prototype.h>

#include <iostream>
#include <string>

// interface class
class IGreeter : Dg::Object<IGreeter> {
public:
    virtual auto greet() -> void = 0;
};

// first prototype class
class SayHi : public Dg::Patterns::Prototype<IGreeter, SayHi> {
    std::string m_name;

public:
    SayHi() = default;
    auto operator=(const SayHi&) -> SayHi& = default;
    auto operator=(SayHi&&) noexcept -> SayHi& = default;
    SayHi(SayHi&&) noexcept = default;
    ~SayHi() override = default;

    // custom copy ctor to showcase the cloning function
    SayHi(const SayHi& origin)
    {
        m_name = origin.m_name + " (clone)";
    }
    SayHi(std::string name)
        : m_name(std::move(name)) {};
    auto greet() -> void override
    {
        std::cout << "Hi, " << m_name << std::endl;
    }
};

// second prototype class
class SayHello : public Dg::Patterns::Prototype<IGreeter, SayHello> {
    std::string m_name;

public:
    SayHello() = default;
    auto operator=(const SayHello&) -> SayHello& = default;
    auto operator=(SayHello&&) noexcept -> SayHello& = default;
    SayHello(SayHello&&) noexcept = default;
    ~SayHello() override = default;

    // custom copy ctor to showcase the cloning function
    SayHello(const SayHello& origin)
    {
        m_name = origin.m_name + " (clone)";
    }
    SayHello(std::string name)
        : m_name(std::move(name)) {};
    auto greet() -> void override
    {
        std::cout << "Hello, " << m_name << std::endl;
    }
};

auto main() -> int
{
    // use prototype interface here
    const Dg::Patterns::IPrototype<IGreeter>::SPtr sayHello = std::make_shared<SayHello>("Nino!");
    const Dg::Patterns::IPrototype<IGreeter>::SPtr sayHi = std::make_shared<SayHi>("Mimi!");

    // showcase cloning via the interface class
    const auto sayHelloClone = sayHello->cloneShared();
    const auto sayHiClone = sayHi->cloneUnique();

    sayHelloClone->greet();
    sayHiClone->greet();

    return EXIT_SUCCESS;
}

// Output:
// Hello, Nino! (clone)
// Hi, Mimi! (clone)