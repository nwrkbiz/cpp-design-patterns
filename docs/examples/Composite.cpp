/**
 * @file Composite.cpp
 * @brief Example showing how to utilize the composite template classes.
 * @author Daniel Giritzer, MSc
 */
// Composite example.
#include <Dg/Patterns/Composite.h>

#include <iostream>
#include <string>

// create an interface for tree nodes
class INode : public Dg::Patterns::Composite<INode> {
public:
    virtual auto greet() -> void = 0;
};

// create concrete nodes of type SayHi
class SayHi : public INode {
    std::string m_name = {};

public:
    SayHi(std::string name)
        : m_name(std::move(name))
    {
    }

    auto greet() -> void override
    {
        std::cout << "Hi, " << m_name << std::endl;
    }
};

// create concrete nodes of type SayHello
class SayHello : public INode {
    std::string m_name = {};

public:
    SayHello(std::string name)
        : m_name(std::move(name))
    {
    }

    auto greet() -> void override
    {
        std::cout << "Hello, " << m_name << std::endl;
    }
};

auto main() -> int
{
    // build tree structure
    auto rootElem = std::make_shared<SayHi>("root");

    auto rootChild1 = std::make_shared<SayHi>("rootChild1");
    auto rootChild2 = std::make_shared<SayHello>("rootChild2");
    auto rootChild3 = std::make_shared<SayHi>("rootChild3");

    auto rootChild11 = std::make_shared<SayHello>("rootChild11");

    auto rootChild21 = std::make_shared<SayHello>("rootChild21");
    auto rootChild22 = std::make_shared<SayHi>("rootChild22");
    auto rootChild23 = std::make_shared<SayHello>("rootChild23");

    rootElem->addChild(rootChild1);
    rootElem->addChild(rootChild2);
    rootElem->addChild(rootChild3);

    rootChild1->addChild(rootChild11);

    rootChild2->addChild(rootChild21);
    rootChild2->addChild(rootChild22);
    rootChild2->addChild(rootChild23);

    for (const auto& elem : *rootElem) {
        elem->greet();
    }

    return EXIT_SUCCESS;
}

// Output:
// Hi, root
// Hi, rootChild1
// Hello, rootChild11
// Hello, rootChild2
// Hello, rootChild21
// Hi, rootChild22
// Hello, rootChild23
// Hi, rootChild3