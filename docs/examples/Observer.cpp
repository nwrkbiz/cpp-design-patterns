/**
 * @file Observer.cpp
 * @brief Example showing how to utilize the observer template classes.
 * @author Daniel Giritzer, MSc
 */
// Observer example.
#include <Dg/Patterns/Observer.h>

#include <iostream>
#include <string>

// observable object
class NameHolder : public Dg::Patterns::Observable<NameHolder> {
    std::string m_name;

public:
    NameHolder(std::string name)
        : m_name(std::move(name))
    {
    }
    [[nodiscard]] auto getName() const -> std::string
    {
        return m_name;
    };
};

// observer #1
class SayHi : public Dg::Patterns::IObserver<NameHolder> {
public:
    auto update(const NameHolder::SPtr& obs) -> void override
    {
        std::cout << "Hi: " << obs->getName() << std::endl;
    }
};

// observer #2
class SayHello : public Dg::Patterns::IObserver<NameHolder> {
public:
    auto update(const NameHolder::SPtr& obs) -> void override
    {
        std::cout << "Hello: " << obs->getName() << std::endl;
    }
};

auto main() -> int
{
    // create the observable object
    auto name = std::make_shared<NameHolder>("Nino");

    // create the obsevers
    auto sayHi = std::make_shared<SayHi>();
    auto sayHello = std::make_shared<SayHello>();

    // subscribe the observers and notify
    name->subscribe(sayHi);
    name->subscribe(sayHello);
    name->notify();

    // unsubscribe one observer and notify again
    name->unsubscribe(sayHello);
    name->notify();
    return EXIT_SUCCESS;
}

// Output:
// Hi: Nino
// Hello: Nino
// Hi: Nino