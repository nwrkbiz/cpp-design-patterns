/**
 * @file PassKey.cpp
 * @brief Example showing how to utilize the pass key class.
 * @author Daniel Giritzer, MSc
 */
// PassKey example.
#include <Dg/Patterns/PassKey.h>

#include <iostream>
#include <string>

class MyClass : public Dg::Object<MyClass> {
    // key which can only be created within this class
    Dg::Patterns::Key<MyClass> m_key;
    std::string m_name = "Nino";

public:
    // Can only be called from this class, because only
    // MyClass can create Key<MyClass> Objects.
    auto locked(const Dg::Patterns::Key<MyClass>& /*key*/) -> void
    {
        std::cout << "Secret, Hello " << m_name << std::endl;
    };

    virtual auto sayHiAndDoWork() -> void final
    {
        std::cout << "Hi!" << std::endl;
        locked(m_key);
        doWork(m_key); // Can only be called within this class
    }

protected:
    // inherit to implement
    virtual void doWork(const Dg::Patterns::Key<MyClass>& /*key*/) = 0;
};

class MyClassImpl : public MyClass {
public:
    MyClassImpl()
    {
        // Key<MyClass> key; // cannot be created
        // locked(key); // cannot be called
        // doWork(key); // cannot be called as well
        sayHiAndDoWork(); // can be called
    }

protected:
    auto doWork(const Dg::Patterns::Key<MyClass>& /*key*/) -> void override
    {
        std::cout << "Doing hard work" << std::endl;
    }
};

auto main() -> int
{
    auto myObj = std::make_shared<MyClassImpl>();
    myObj->sayHiAndDoWork(); // only callable function
    return EXIT_SUCCESS;
}

// Output:
// Hi!
// Secret, Hello Nino
// Doing hard work
// Hi!
// Secret, Hello Nino
// Doing hard work