/**
 * @file Singleton.cpp
 * @brief Example showing how to utilize the singleton template class.
 * @author Daniel Giritzer, MSc
 */
// Singleton example.
#include <Dg/Patterns/Singleton.h>

#include <iostream>

// Class Implementation
class MyClass : public Dg::Patterns::Singleton<MyClass> {
public:
    // Class implementation belongs here
    auto identify() -> void
    {
        std::cout << "Hi, i am " << this << std::endl;
    }

    ~MyClass() override
    {
        std::cout << "Destroyed!" << std::endl;
    }

    MyClass(const MyClass&) = default;
    auto operator=(const MyClass&) -> MyClass& = default;
    auto operator=(MyClass&&) noexcept -> MyClass& = default;
    MyClass(MyClass&&) noexcept = default;

protected:
    // protect ctors, only Singleton may call it
    MyClass()
    {
        std::cout << "Created!" << std::endl;
    }

    MyClass(const std::string& info)
    {
        std::cout << "Created! " << info << std::endl;
    }
    friend Dg::Patterns::Singleton<MyClass>;
};

auto main() -> int
{
    MyClass::getInstance("Info")->identify();
    MyClass::getInstance()->identify();

    return EXIT_SUCCESS;
}

// Output:
// Created! Info
// Hi, i am 0x56459b806eb0
// Hi, i am 0x56459b806eb0
// Destroyed!