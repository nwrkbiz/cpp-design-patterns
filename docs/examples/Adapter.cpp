/**
 * @file Adapter.cpp
 * @brief Example showing how to utilize the adapter template classes.
 * @author Daniel Giritzer, MSc
 */
// Adapter example.
#include <Dg/Patterns/Adapter.h>

#include <iostream>
#include <string>

// Interface of our "API" class, the interface we should adapt to
class IGreeter : Dg::Object<IGreeter> {
public:
    virtual auto greet() -> void = 0;
};

// Interface which needs to be adapted (A client may only be able to use IGreeter classes.)
class IGreeterUnknown : Dg::Object<IGreeterUnknown> {
public:
    virtual auto greetUnknown() -> void = 0;
};

// Implementation of our class, this is the adaptee
class SayHi : public IGreeterUnknown {
public:
    auto greetUnknown() -> void override
    {
        std::cout << "Hi!" << std::endl;
    }
};

// Implementation of our adapter
class MyAdapter : public IGreeter, public Dg::Patterns::Adapter<IGreeterUnknown> {
public:
    // map/adapt the greet function to the unknown function
    auto greet() -> void override
    {
        if (getAdaptee()) {
            getAdaptee()->greetUnknown();
        }
    }
};

auto main() -> int
{
    auto adapter = std::make_shared<MyAdapter>();
    adapter->setAdaptee(std::make_shared<SayHi>());

    adapter->greet();
    return EXIT_SUCCESS;
}

// Output:
// Hi!