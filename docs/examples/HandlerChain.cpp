/**
 * @file HandlerChain.cpp
 * @brief Example showing how to utilize the chain of responsibility template classes.
 * @author Daniel Giritzer, MSc
 */
// Chain of responsibility example.
#include <Dg/Patterns/HandlerChain.h>

#include <iostream>
#include <string>

// create own intermediate interface (this allows us to separate different kind of handler chains)
class IMyHandler : public Dg::Patterns::Handler<IMyHandler, int&> {
};

class Plus : public IMyHandler {
public:
    auto handle(int& val) -> bool override
    {
        val++;
        std::cout << "Plus" << std::endl;
        return true;
    }
};

class Minus : public IMyHandler {
public:
    auto handle(int& val) -> bool override
    {
        val--;
        std::cout << "Minus" << std::endl;
        return true;
    }
};

auto main() -> int
{
    const auto firstHandler = IMyHandler::createHandlerChain(
        std::make_shared<Minus>(),
        std::make_shared<Plus>(),
        std::make_shared<Plus>());

    int val = 0;

    firstHandler->startHandling(val);

    std::cout << "Val is: " << val << std::endl;

    return EXIT_SUCCESS;
}

// Output:
// Minus
// Plus
// Plus
// Val is: 1
