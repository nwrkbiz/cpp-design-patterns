/**
 * @file Visitor.cpp
 * @brief Example showing how to utilize the visitor template classes.
 * @author Daniel Giritzer, MSc
 */
// Visitor example.
#include <Dg/Patterns/Visitor.h>

#include <iostream>
#include <string>

// Visitable "SayHi" Object
class SayHi : public Dg::Patterns::Visitable<SayHi> {
    std::string m_name = {};

public:
    SayHi(std::string name)
        : m_name(std::move(name))
    {
    }
    auto sayHi() -> void
    {
        std::cout << "Hi, " << m_name << std::endl;
    }
};

// Visitable "SayHello" Object
class SayHello : public Dg::Patterns::Visitable<SayHello> {
    std::string m_name = {};

public:
    SayHello(std::string name)
        : m_name(std::move(name))
    {
    }
    auto sayHello() -> void
    {
        std::cout << "Hello, " << m_name << std::endl;
    }
};

// Implementation of visitor which is able to visit "SayHi" and "SayHello" objects
class MyVisitor : public Dg::Patterns::IVisitor<SayHi, SayHello> {
public:
    // Visit function for SayHi objects
    auto visit(const std::shared_ptr<SayHi>& elem) -> void override
    {
        elem->sayHi();
    }

    // Visit function for SayHello objects
    auto visit(const std::shared_ptr<SayHello>& elem) -> void override
    {
        elem->sayHello();
    }
};

auto main() -> int
{
    // create the visitor
    auto visitor = std::make_shared<MyVisitor>();

    // create our visitable objects
    auto sayHiElem = std::make_shared<SayHi>("Nino");
    auto sayHelloElem = std::make_shared<SayHello>("Mimi");

    // let our visitor visit the objects
    sayHiElem->doVisit(visitor);
    sayHelloElem->doVisit(visitor);

    return EXIT_SUCCESS;
}

// Output:
// Hi, Nino
// Hello, Mimi