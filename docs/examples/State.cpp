/**
 * @file State.cpp
 * @brief Example showing how to utilize the state template classes.
 * @author Daniel Giritzer, MSc
 */
// State example.
#include <Dg/Patterns/State.h>

#include <iostream>
#include <string>

// Create state machine class (a very simple one for showcase purposes)
class MyStateMachine : public Dg::Patterns::StateContext<MyStateMachine, int&> {
};

class EndState : public Dg::Patterns::IState<MyStateMachine, int&> {
    auto executeState(const MyStateMachine::SPtr& /* ctx */, int& /* val */) -> void override
    {
        // last state, do not change state
    }
};

// subtract state
class SubState : public Dg::Patterns::IState<MyStateMachine, int&> {
    auto executeState(const MyStateMachine::SPtr& ctx, int& val) -> void override
    {
        val--;
        ctx->setState(std::make_shared<EndState>());
    }
};

// add state
class AddState : public Dg::Patterns::IState<MyStateMachine, int&> {
public:
    auto executeState(const MyStateMachine::SPtr& ctx, int& val) -> void override
    {
        val++;
        ctx->setState(std::make_shared<SubState>());
    }
};

auto main() -> int
{
    int myVal = 1;

    const auto stateMachine = std::make_shared<MyStateMachine>();
    stateMachine->setState(std::make_shared<AddState>());

    stateMachine->executeState(myVal); // Adds
    stateMachine->executeState(myVal); // Subtracts
    stateMachine->executeState(myVal); // End state, does nothing
    stateMachine->executeState(myVal); // End state, does nothing

    std::cout << "Val: " << myVal << std::endl;

    return EXIT_SUCCESS;
}

// Output:
// Val: 1