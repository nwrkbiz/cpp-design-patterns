/**
 * @file Mediator.cpp
 * @brief Example showing how to utilize the mediator template class.
 * @author Daniel Giritzer, MSc
 */
// Mediator example.
#include <Dg/Patterns/Mediator.h>

#include <iostream>
#include <string>

// component #1
class SendHi : public Dg::Patterns::Mediatable<const std::string&, SendHi> {
    std::string m_name;

public:
    auto sendHi(const std::string& name) -> void
    {
        if (getMediator()) {
            getMediator()->mediate(shared_from_this(), "print");
        }
        m_name = name;
    }
    auto getName() -> std::string
    {
        return m_name;
    }
    using SPtr = std::shared_ptr<SendHi>;
};

// component #2
class SendHello : public Dg::Patterns::Mediatable<const std::string&, SendHello> {
public:
    auto sendHello() -> void
    {
        if (getMediator()) {
            getMediator()->mediate(shared_from_this(), "print");
        }
    }
    using SPtr = std::shared_ptr<SendHello>;
};

// Mediator class, which can mediate between the objects
// we need to implement a mediate function for every mediatable object here
class MyMediator : public Dg::Patterns::IMediator<const std::string&, SendHello, SendHi> {
public:
    auto mediate(const SendHi::SPtr& component, const std::string& command) -> void override
    {
        if (command == "print" && component) {
            std::cout << "Hi" << std::endl;
            // here the mediator can also interact with the components, (e.g. pass data between them)
        }
    }

    auto mediate(const SendHello::SPtr& component, const std::string& command) -> void override
    {
        if (command == "print" && component) {
            std::cout << "Hello" << std::endl;
            // here the mediator can also interact with the mediatable components, (e.g. pass data between them)
            // like so:
            SendHi::SPtr sendHi = nullptr;
            getMediatable(sendHi);
            if (sendHi) {
                sendHi->sendHi("Nino!");
            }
        }
    }
};

auto main() -> int
{
    // create components and the mediator
    auto mediator = std::make_shared<MyMediator>();
    auto sendHi = std::make_shared<SendHi>();
    auto sendHello = std::make_shared<SendHello>();

    // add the mediator to the components
    sendHi->setMediator(mediator);
    sendHello->setMediator(mediator);

    // test mediation
    sendHi->sendHi("Mimi!");
    std::cout << sendHi->getName() << std::endl;
    sendHello->sendHello();
    std::cout << sendHi->getName() << std::endl;

    return EXIT_SUCCESS;
}

// Output:
// Hi
// Mimi!
// Hello
// Hi
// Nino!