/**
 * @file Decorator.cpp
 * @brief Example showing how to utilize the decorator template classes.
 * @author Daniel Giritzer, MSc
 */
// Decorator example.
#include <Dg/Patterns/Decorator.h>

#include <iostream>
#include <string>

// Interface of our class
class IGreeter : Dg::Object<IGreeter> {
public:
    virtual auto greet() -> void = 0;
};

// Implementation of our class, this is the decoratee
class SayHi : public IGreeter {
public:
    auto greet() -> void override
    {
        std::cout << "Hi!" << std::endl;
    }
};

// Implementation of our decorator
class MyDecorator : public Dg::Patterns::Decorator<IGreeter> {
public:
    // the function we want to decorate, pass the call
    // to 1:1 to the decoratee
    auto greet() -> void override
    {
        if (!getDecoratee()) {
            return;
        }
        getDecoratee()->greet();
    }
};

// one concrete decorator
class DecorateBye : public MyDecorator {
public:
    auto greet() -> void override
    {
        MyDecorator::greet(); // execute the original function first
        bye(); // execute "decoration"
    }

    static auto bye() -> void
    {
        std::cout << "Bye!" << std::endl;
    }
};

// another concrete decorator
class DecorateCiao : public MyDecorator {
public:
    auto greet() -> void override
    {
        MyDecorator::greet(); // execute the original function first
        ciao(); // execute "decoration"
    }

    static auto ciao() -> void
    {
        std::cout << "Ciao!" << std::endl;
    }
};

auto main() -> int
{
    // create a class we want to decorate
    auto sayHi = std::make_shared<SayHi>();

    // decorate our hi object with the decorateBye object
    auto decorateBye = std::make_shared<DecorateBye>();
    decorateBye->setDecoratee(sayHi);

    // we can also decorate our decorator
    auto decorateCiao = std::make_shared<DecorateCiao>();
    decorateCiao->setDecoratee(decorateBye);

    // execute the now "decorated" function
    decorateCiao->greet();
    return EXIT_SUCCESS;
}

// Output:
// Hi!
// Bye!
// Ciao!