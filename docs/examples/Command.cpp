/**
 * @file Command.cpp
 * @brief Example showing how to utilize the command template classes.
 * @author Daniel Giritzer, MSc
 */
// Command example.
#include <Dg/Patterns/Command.h>

#include <functional>
#include <iostream>

// interface for our receiver object (-> object receiving commands)
class IReceiver : public Dg::Object<IReceiver> {
public:
    virtual auto performAction() -> void = 0;
    virtual auto undoAction() -> void = 0;
};

// implementation of the receiver object, which shall receive the commands
class Receiver : public IReceiver {
    auto performAction() -> void override
    {
        std::cout << "Perform Action!" << std::endl;
    }
    auto undoAction() -> void override
    {
        std::cout << "Undo Action!" << std::endl;
    }
};

// create own intermediate interface (this allows us to separate different kind of invokers/commands)
class IMyCommand : public Dg::Patterns::ICommand<IMyCommand> {
};

// command which executes the receivers functions
class MyCommand : public IMyCommand {
    IReceiver::SPtr m_receiver;

public:
    MyCommand(IReceiver::SPtr receiver)
        : m_receiver(std::move(receiver)) {};

    auto execute() -> void override
    {
        m_receiver->performAction();
    }

    auto undo() -> void override
    {
        m_receiver->undoAction();
    }
};

// command which uses a lambda function as receiver
class SimpleCommand : public IMyCommand {
    std::function<void(void)> m_receiver;

public:
    SimpleCommand(std::function<void(void)> rec)
        : m_receiver(std::move(rec)) {};
    auto execute() -> void override
    {
        m_receiver();
    }

    auto undo() -> void override
    {
        std::cout << "Undo Simple!" << std::endl;
    }
};

auto main() -> int
{
    // create an invoker
    auto invoker = std::make_shared<Dg::Patterns::Invoker<IMyCommand>>();

    // add command for the receiver to the invoker
    invoker->executeCommand(std::make_shared<MyCommand>(std::make_shared<Receiver>()));
    invoker->executeCommand(std::make_shared<SimpleCommand>([]() -> void { std::cout << "Simple Action!" << std::endl; }));

    // execute and undo the command
    invoker->undoCommand();
    invoker->undoCommand();

    return EXIT_SUCCESS;
}

// Output:
// Perform Action!
// Simple Action!
// Undo Simple!
// Undo Action!