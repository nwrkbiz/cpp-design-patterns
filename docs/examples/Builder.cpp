/**
 * @file Builder.cpp
 * @brief Example showing how to utilize the builder template classes.
 * @author Daniel Giritzer, MSc
 */
// Builder example.
#include <Dg/Patterns/Builder.h>

#include <iostream>
#include <string>

// interface for the immutable class
class IGreeter : public Dg::Object<IGreeter> {
public:
    virtual auto greet() -> void = 0;
};

// interface for the builder class
class IGreetBuilder : public Dg::Object<IGreetBuilder> {
public:
    [[nodiscard]] virtual auto getGreeting() const -> std::string = 0;
    [[nodiscard]] virtual auto getName() const -> std::string = 0;
    virtual auto setGreeting(const std::string& greeting) -> std::shared_ptr<Dg::Patterns::IBuilder<IGreetBuilder, IGreeter>> = 0;
    virtual auto setName(const std::string& name) -> std::shared_ptr<Dg::Patterns::IBuilder<IGreetBuilder, IGreeter>> = 0;
};

// implementation of the immutable class
class Greeter : public IGreeter {
    std::string m_greeting;
    std::string m_name;

public:
    Greeter(const Dg::Patterns::IBuilder<IGreetBuilder, IGreeter>::SPtr& builder)
        : m_greeting(builder->getGreeting())
        , m_name(builder->getName())
    {
    }

    auto greet() -> void override
    {
        std::cout << m_greeting << ", " << m_name << "!" << std::endl;
    }
};

// builder class
class GreetBuilder : public Dg::Patterns::Builder<IGreetBuilder, IGreeter, Greeter> {
    std::string m_greeting;
    std::string m_name;

public:
    using SPtr = std::shared_ptr<GreetBuilder>;

    [[nodiscard]] auto getGreeting() const -> std::string override
    {
        return m_greeting;
    };

    [[nodiscard]] auto getName() const -> std::string override
    {
        return m_name;
    };

    auto setGreeting(const std::string& greeting) -> Dg::Patterns::IBuilder<IGreetBuilder, IGreeter>::SPtr override
    {
        m_greeting = greeting;
        return this->shared_from_this();
    };

    auto setName(const std::string& name) -> Dg::Patterns::IBuilder<IGreetBuilder, IGreeter>::SPtr override
    {
        m_name = name;
        return this->shared_from_this();
    };
};

auto main() -> int
{
    // showcase the use of the interface here:
    const Dg::Patterns::IBuilder<IGreetBuilder, IGreeter>::SPtr builder = std::make_shared<GreetBuilder>();

    // build immutable greeter object
    const auto greeter1 = builder->setGreeting("Hi")->setName("Nino")->buildUnique();
    const auto greeter2 = builder->setGreeting("Hello")->setName("Mimi")->buildUnique();

    greeter1->greet();
    greeter2->greet();

    return EXIT_SUCCESS;
}

// Output:
// Hi, Nino!
// Hello, Mimi!