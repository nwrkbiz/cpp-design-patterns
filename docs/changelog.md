# Changelog



# 2.0.0 (2024-07-17)


### Code Refactoring

* move include path to reflect namespaces ([14d547b](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/14d547b95ef09cb78c5917d7119b52692e04ccc5))


### BREAKING CHANGES

* include paths need to be adjusted from `patterns` to `Dg/Patterns`



# 1.1.0 (2024-05-02)


### Bug Fixes

* **ci:** fix non working no-pipelines.sh script ([e9216da](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/e9216daf3358ddeb08e2194a4393b295037d953f))
* **devcontainer:** adjusted devcontainer.json file to match the newest standard ([60b1437](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/60b14373ac6fc4ca4ec3dc260e61fed478c3d9d1))


### Features

* **Observer:** allow user to check whether subscribing or unsubscribing was successful ([a2a9325](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/a2a93259b1a12268c4ea9abfb960b1bdc513c4ab)), closes [#10](https://gitlab.com/nwrkbiz/cpp-design-patterns/issues/10)
* **Property:** add the property idiom ([26fef24](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/26fef24647060831c49184098a84d2d72ecf8ffc))
* **Property:** allow setter access to readonly auto property ([8131218](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/8131218cfe5ea2a2876d5087ed2a98076fb233e3))



## 1.0.1 (2024-02-01)


### Bug Fixes

* add nodiscard to createRaw and buildRaw functions ([a8a7b58](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/a8a7b58a3a97bde0f156dd9087eb73ffeea4c352)), closes [#1](https://gitlab.com/nwrkbiz/cpp-design-patterns/issues/1)
* **cmake:** set c++ standard to c++17 in cmake files ([441e199](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/441e1996d2689a6c93ed1ddbab2280def0b4819e)), closes [#3](https://gitlab.com/nwrkbiz/cpp-design-patterns/issues/3)
* **docs:** redraw adapter example UML diagram to better reflect code ([d4e47cf](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/d4e47cf8f1e0e66aa58256741a5562d312580d57)), closes [#2](https://gitlab.com/nwrkbiz/cpp-design-patterns/issues/2)
* **docs:** underline functions in singleton example UML diagram ([536abd5](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/536abd53623253e56cc2e4bd176ccfa791c0d6be)), closes [#6](https://gitlab.com/nwrkbiz/cpp-design-patterns/issues/6)
* **scripts:** fix helper scripts so that they find the right files ([b81d6fd](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/b81d6fdd2fcac393b491e8725387c99cf822b291)), closes [#9](https://gitlab.com/nwrkbiz/cpp-design-patterns/issues/9)
* **singleton:** make creation of singleton instance threadsafe ([15eaa10](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/15eaa1046fd7a2c199f01e2f57e210b1781ae7a4)), closes [#5](https://gitlab.com/nwrkbiz/cpp-design-patterns/issues/5)



# 1.0.0 (2024-01-07)


### Bug Fixes

* spelling of mediatable was wrong ([c86e4b1](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/c86e4b1c11d8331b956e19cad2b25152076c5e40))


### Features

* add chain of responsibility pattern ([4058a63](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/4058a638422870b46dec34d610fd6364c0a4cbc8))
* add prototype pattern ([c1e5cee](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/c1e5ceec26565ee452baddded0fed414086bc0e1))
* add state pattern ([18326a4](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/18326a4845751f824418269dbd7d02aee56ed79a))
* adjust ecosystem to ignore git submodules if any would exist ([1eff2cf](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/1eff2cff032587b516c537141524ff24b62da5ec))
* implement builder pattern ([5ea120d](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/5ea120d66e0300dbfc05a3179f05008eaa5c40fa))
* implement feedback ([a1ff3bd](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/a1ff3bdd2447bb8e6f1cd863fd8419337ae304d9))
* initial commit ([a2f0be5](https://gitlab.com/nwrkbiz/cpp-design-patterns/commit/a2f0be586026f9955810dad9cf4b17a3db8208cd))



