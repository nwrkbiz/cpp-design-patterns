Introduction
============

This project contains a collection of header only template classes, which implement the skeletons of some well know design patterns.
The purpose of this project is, to reduce the amount of code needed to implement these patterns. This library requires at least a C++17-compliant compiler.

To get an overview on all patterns, take a look at the file list (check the "Files" menu on the left).

List of Patterns
----------------

Following patterns and idioms are provided by this project:

### Structural Patterns

* Adapter.h
* Composite.h
* Decorator.h

### Creational Patterns

* Builder.h
* FactoryMethod.h
* Prototype.h
* SimpleFactory.h
* Singleton.h

### Behavioral Patterns

* Command.h
* HandlerChain.h
* Mediator.h
* Memento.h
* Observer.h
* PassKey.h
* Property.h
* State.h
* Visitor.h

Quick Start
-----------

You can add this library to your CMake project using e.g. FetchContent. Here an example CMakeLists.txt file:

### CMakeLists.txt

``````
cmake_minimum_required(VERSION 3.25)

include(FetchContent)

project(Example)

set(TARGET_NAME example)
add_executable(${TARGET_NAME} main.cpp)

# set c++ standard to at least C++17
set_property(TARGET ${TARGET_NAME} PROPERTY CXX_STANDARD 17)

# fetch the library if not existent
if(NOT TARGET Patterns)
    set(PATTERN_LIBRARY_VERSION "2.0.0" CACHE STRING "Version of the C++ pattern library to use.")
    message("C++ pattern library was not found! Trying to download library version ${PATTERN_LIBRARY_VERSION} instead!")
    FetchContent_Declare(patterns URL https://gitlab.com/nwrkbiz/cpp-design-patterns/-/archive/v${PATTERN_LIBRARY_VERSION}/cpp-design-patterns-v${PATTERN_LIBRARY_VERSION}.zip)
    FetchContent_GetProperties(patterns)
    if(NOT patterns)
      FetchContent_Populate(patterns)
      add_subdirectory(${patterns_SOURCE_DIR} ${patterns_BINARY_DIR} EXCLUDE_FROM_ALL)
    endif()
endif()

# use the library with our target
target_link_libraries(${TARGET_NAME} PRIVATE Patterns)
``````

### Content of main.cpp

``````
// include the header you want to use here
#include <Dg/Patterns/Adapter.h>

// .. your code

auto main() -> int {
    // ... your code
    return 0;
}
``````

Citations and References
------------------------

* Book: Design Patterns: Elements of Reusable Object-Oriented Software, 1994, E. Gamma, R. Helm, R. Johnson, and J. Vlissides.
* Web: [Passkey Idiom: More Useful Empty Classes](https://arne-mertz.de/2016/10/passkey-idiom/), 2016, Arne Mertz, (Visited 08.03.2024)
* Web: [C++ properties, read only and/or dynamic getters](https://github.com/oskarirauta/propertycpp), 2022, Oskari Rauta (Visited 08.03.2024)

Testing and Quality Assurance
------------------------------

This library has been automatically tested. The testing report can be found here: [Testing report](./coverage_report/coverage_report.html)