# Directory of this file for later usage
set(FUNCTIONS_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR})  

# Default build type
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING
            "Default build type: RelWithDebInfo" FORCE)
endif()

# make sure the output dir is the same, no matter
# which generator is used
if(NOT CMAKE_GENERATOR MATCHES "^Visual*.")
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/$<CONFIG>)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/$<CONFIG>)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/$<CONFIG>)
endif()

# find all git submodules (to exclude them from doxygen)
set(DOXYGEN_EXCLUDE_DIR "")
execute_process(COMMAND bash ${FUNCTIONS_CMAKE_DIR}/../scripts/list_submodules.sh
                WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
                OUTPUT_VARIABLE GIT_SUBMODULES
                OUTPUT_STRIP_TRAILING_WHITESPACE)
if(DEFINED GIT_SUBMODULES)
    if(NOT "${GIT_SUBMODULES} " STREQUAL " ")
        string(REPLACE "\n" ";" GIT_SUBMODULES_LIST ${GIT_SUBMODULES})
        foreach(SUBMODULE_PATH ${GIT_SUBMODULES_LIST})
            set(DOXYGEN_EXCLUDE_DIR "${DOXYGEN_EXCLUDE_DIR}EXCLUDE+=${SUBMODULE_PATH}\n")
        endforeach()
    endif()
endif()

#! enable_clang_tidy : Enables clang tidy static code analys for the given
# target
# 
# \arg:TARGET_NAME Name of the target
function(enable_clang_tidy TARGET_NAME)
    # clang-tidy static code analysis
    set(ENABLE_CLANG_TIDY FALSE CACHE BOOL "Enable static code analysis using clang tidy (default off).")
    if(ENABLE_CLANG_TIDY)
        find_program(CLANG_TIDY_EXE NAMES "clang-tidy")
        set(CLANG_TIDY_CONFIG_FILE ${FUNCTIONS_CMAKE_DIR}/.clang-tidy)
        set(CLANG_TIDY_COMMAND "${CLANG_TIDY_EXE}" "--config-file=${CLANG_TIDY_CONFIG_FILE}")
        set_target_properties(${TARGET_NAME} PROPERTIES CXX_CLANG_TIDY "${CLANG_TIDY_COMMAND}")
        message(STATUS "Enable static code analysis for target '${TARGET_NAME}' using clang-tidy")
    endif()
endfunction()

#! add_test_target : Helper function, adds unit tests to given targets.
# Is indirectly called by add_target.
# 
# \arg:TEST_NAME Name of the unittest executable to be created.
# \arg:TARGET_TYPE Type of target (DYNAMIC_LIBRARY, STATIC_LIBRARY, EXECUTABLE).
# \arg:TARGET_NAME Name of the target to add unittests for.
# \arg:TEST_SRC Source files for this unit test (make sure to pass variables containing lists under quotes).
function(add_test_target TEST_NAME TARGET_TYPE TARGET_NAME TESTS_SRC)
    if(ENABLE_TESTS)

    if(${TARGET_TYPE} STREQUAL "STATIC_LIBRARY" OR 
       ${TARGET_TYPE} STREQUAL "DYNAMIC_LIBRARY" OR 
       ${TARGET_TYPE} STREQUAL "INTERFACE" )
            find_package(GTest)
            if(NOT TARGET GTest::gtest_main)
                include(FetchContent)
                set(GTEST_LIBRARY_VERSION "1.13.0" CACHE STRING "Version of the GTest library to use, if none is found on the system.")
                FetchContent_Declare(
                    googletest
                    URL https://github.com/google/googletest/archive/refs/tags/v${GTEST_LIBRARY_VERSION}.zip
                )
                FetchContent_GetProperties(googletest)
                if(NOT googletest_POPULATED)
                    message("GTest library was not found! Trying to download GTest library ${GTEST_LIBRARY_VERSION} instead!")
                    FetchContent_Populate(googletest)
                    add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR} EXCLUDE_FROM_ALL)
                endif()
            endif()

            add_executable("${TEST_NAME}" ${TESTS_SRC})

            include(CheckIncludeFile)
            check_include_file("fff.h" HAVE_FFF_H)
            if(NOT HAVE_FFF_H AND NOT fff_POPULATED)
                include(FetchContent)
                set(FFF_LIBRARY_VERSION "1.1" CACHE STRING "Version of the FFF library to use, if none is found on the system.")
                FetchContent_Declare(
                    fff
                    URL https://github.com/meekrosoft/fff/releases/download/v${FFF_LIBRARY_VERSION}/fff.h
                    DOWNLOAD_NO_EXTRACT TRUE
                )
                FetchContent_GetProperties(fff)
                if(NOT ${fff_POPULATED})
                    message("FFF library was not found! Trying to download FFF library ${FFF_LIBRARY_VERSION} instead!")
                    FetchContent_Populate(fff)
                endif()
            endif()

            target_link_libraries("${TEST_NAME}" PRIVATE ${TARGET_NAME} GTest::gtest_main GTest::gtest GTest::gmock GTest::gmock_main)
            target_link_libraries("${TEST_NAME}" PRIVATE "-lgcov")
            target_include_directories("${TEST_NAME}" PRIVATE "tests" "${fff_SOURCE_DIR}")
            target_compile_options("${TEST_NAME}" PRIVATE "--coverage")
            target_link_options("${TEST_NAME}" PRIVATE "--coverage")
            set_property(TARGET "${TEST_NAME}" PROPERTY CXX_STANDARD 17)
            if(NOT ${TARGET_TYPE} STREQUAL "INTERFACE")
                target_link_libraries("${TARGET_NAME}" PRIVATE "-lgcov")
                target_compile_options("${TARGET_NAME}" PRIVATE "--coverage")
                target_link_options("${TARGET_NAME}" PRIVATE "--coverage")
            endif()

            include(GoogleTest)
            gtest_discover_tests("${TEST_NAME}" WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

            add_dependencies(ALL_TESTS ${TARGET_NAME})
        endif()
    endif()
endfunction()

#! add_target : Add new build target with default options.
#
# Source files from "TARGET_DIR/src/**" are automatically added.
# Include dir "TARGET_DIR/include" is automatically added as PUBLIC include dir.
# Include dir "TARGET_DIR/private_include" is automatically added as PRIVATE include dir.
#
# \arg:TARGET_NAME Name of the target
# \arg:TARGET_TYPE Type of target (DYNAMIC_LIBRARY, STATIC_LIBRARY, EXECUTABLE, INTERFACE).
# \arg:ATOMIC_TESTS Optional, If true one unittests executable is built for each unittest cpp file, otherwise only one unittest executable is built (defaults to false).
function(add_target TARGET_NAME TARGET_TYPE)

    set(ENABLE_TESTS FALSE CACHE BOOL "Build unit tests (default off).")
    if(ENABLE_TESTS)
        set(ENABLE_MEMCHECK FALSE CACHE BOOL "Perform a memory check on the unit tests (may detect memory leaks, default off).")
        if(ENABLE_MEMCHECK)
            find_program(MEMORYCHECK_COMMAND valgrind)
            set(MEMORYCHECK_COMMAND_OPTIONS "--leak-check=full --show-leak-kinds=all --error-exitcode=1")
        endif()
        include(CTest)
    endif()

    # parse optional atomic test value
    if(${ARGC} EQUAL 3)
        set(ATOMIC_TESTS ${ARGV2})
    else()
        set(ATOMIC_TESTS false)
    endif()

    file(GLOB TARGET_SRC CONFIGURE_DEPENDS "src/*.cpp" "src/**/*.cpp" "src/**/**/*.cpp" "src/*.c" "src/**/*.c" "src/**/**/*.c")

    if(${TARGET_TYPE} STREQUAL "STATIC_LIBRARY")
        add_library(${TARGET_NAME} STATIC ${TARGET_SRC})
        target_include_directories(${TARGET_NAME} PUBLIC include)
        target_include_directories(${TARGET_NAME} PRIVATE private_include)
    elseif(${TARGET_TYPE} STREQUAL "EXECUTABLE")
        add_executable(${TARGET_NAME} ${TARGET_SRC})
        target_include_directories(${TARGET_NAME} PUBLIC include)
        target_include_directories(${TARGET_NAME} PRIVATE private_include)
    elseif(${TARGET_TYPE} STREQUAL "DYNAMIC_LIBRARY")
        add_library(${TARGET_NAME} SHARED ${TARGET_SRC})
        target_include_directories(${TARGET_NAME} PUBLIC include)
        target_include_directories(${TARGET_NAME} PRIVATE private_include)
    elseif(${TARGET_TYPE} STREQUAL "INTERFACE")
        add_library(${TARGET_NAME} INTERFACE)
        target_include_directories(${TARGET_NAME} INTERFACE include)
    else()
        message(FATAL_ERROR "Wrong target type passed!")
    endif()

    set_property(TARGET ${TARGET_NAME} PROPERTY CXX_STANDARD 17)

    enable_clang_tidy(${TARGET_NAME})

    if(NOT ${TARGET_TYPE} STREQUAL "INTERFACE")
        ## Basic information hiding
        target_compile_options(${TARGET_NAME} PRIVATE -fvisibility=hidden)
        target_compile_options(${TARGET_NAME} PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-fvisibility-inlines-hidden>)

        ## Enforce strict compiler options (for higher code quality)
        target_compile_options(${TARGET_NAME} PRIVATE -Weffc++)
        target_compile_options(${TARGET_NAME} PRIVATE -Werror)
        target_compile_options(${TARGET_NAME} PRIVATE -Wall)
        target_compile_options(${TARGET_NAME} PRIVATE -Wextra)

        ## Enable some security features
        # address space layout randomization
        target_compile_options(${TARGET_NAME} PRIVATE -fpie)
        target_link_options(${TARGET_NAME} PRIVATE -pie)

        target_compile_options(${TARGET_NAME} PRIVATE -fexceptions)

        # fixes lazy binding issues
        if (${CMAKE_SYSTEM_NAME} STREQUAL Linux)
            target_link_options(${TARGET_NAME} PRIVATE -Wl,-z,relro,-z,now)
        endif()
    endif()

    # Store debug symbols in separate file on Linux
    if(${TARGET_TYPE} STREQUAL "EXECUTABLE" OR
       ${TARGET_TYPE} STREQUAL "DYNAMIC_LIBRARY")
        if (${CMAKE_SYSTEM_NAME} STREQUAL Linux)
            ADD_CUSTOM_COMMAND(TARGET ${TARGET_NAME} POST_BUILD
                COMMAND ${CMAKE_OBJCOPY} --only-keep-debug $<TARGET_FILE:${TARGET_NAME}> ${CMAKE_BINARY_DIR}/$<CONFIG>/${TARGET_NAME}.debug
                COMMAND ${CMAKE_STRIP} -g $<TARGET_FILE:${TARGET_NAME}>
                COMMAND ${CMAKE_OBJCOPY} --add-gnu-debuglink ${CMAKE_BINARY_DIR}/$<CONFIG>/${TARGET_NAME}.debug $<TARGET_FILE:${TARGET_NAME}>)
        endif()
    endif()

    # add unittests
    if(ENABLE_TESTS)
        file(GLOB TESTS_SRC_LIST CONFIGURE_DEPENDS "tests/*.cpp" "tests/**/*.cpp" "tests/**/**/*.cpp")

        if (NOT TARGET ALL_TESTS)
            add_custom_target(ALL_TESTS)
        endif()

        if(ATOMIC_TESTS)
            foreach(TEST_FILE ${TESTS_SRC_LIST})
                set(TEST_FILE_NAME "none")
                cmake_path(GET TEST_FILE FILENAME TEST_FILE_NAME)
                cmake_path(REMOVE_EXTENSION TEST_FILE_NAME)
                if(NOT TEST_FILE_NAME STREQUAL "none")
                    add_test_target(TEST_${TEST_FILE_NAME} ${TARGET_TYPE} ${TARGET_NAME} ${TEST_FILE})
                endif()
            endforeach()
        else()
            add_test_target(TESTS_${TARGET_NAME} ${TARGET_TYPE} ${TARGET_NAME} "${TESTS_SRC_LIST}")
        endif()
    endif()

    if(NOT ${TARGET_TYPE} STREQUAL "INTERFACE")
        configure_file(${FUNCTIONS_CMAKE_DIR}/config.h.in config-${CMAKE_PROJECT_NAME}-${PROJECT_NAME}.h)
        target_include_directories(${TARGET_NAME} PUBLIC "${PROJECT_BINARY_DIR}")
    endif()

    set_property(TARGET ${TARGET_NAME} PROPERTY CXX_STANDARD 17)
endfunction()

#! get_version_info : This function gets version info variables with the help of git.
#
# For this to work use semantic versioning in git tags with the following
# syntax: v1.0.0.0 or v1.0.0 (recommended)
#
# \arg:prj_version Variable to set a version string that can be passed on to a project() call.
# \arg:prj_year Variable to set the current year.
# \arg:prj_timestamp Variable to set the current timestamp.
# \arg:prj_revision Variable to set the git revision (including a dirty marking if there are uncommitted changes).
function(get_version_info prj_version prj_year prj_timestamp prj_revision)

    # Internally used variables:
    # YEAR - current year
    # TIMESTAMP - current timestamp
    # GIT_REVISION - if a git repo the revision hash
    # VERSION_STRING - if a tag exists in format v0.0.0.0 a version string
    string(TIMESTAMP YEAR "%Y")
    string(TIMESTAMP TODAY)
    find_package(Git)
    if(GIT_FOUND)
        # check for git repo
        execute_process(
            COMMAND ${GIT_EXECUTABLE} status
            WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
            RESULT_VARIABLE RC
            OUTPUT_VARIABLE GIT_REVISION
            ERROR_QUIET
            OUTPUT_STRIP_TRAILING_WHITESPACE
        )

        if(RC MATCHES 0)
            # set git revision variable
            execute_process(
            COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
            WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
            OUTPUT_VARIABLE GIT_REVISION
            ERROR_QUIET
            OUTPUT_STRIP_TRAILING_WHITESPACE
            )

            execute_process(
            COMMAND ${GIT_EXECUTABLE} describe --tags --abbrev=0
            WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
            OUTPUT_VARIABLE LAST_TAG
            OUTPUT_STRIP_TRAILING_WHITESPACE 
            ERROR_QUIET
            RESULT_VARIABLE RC
            )
            if(RC MATCHES 0)
                message(STATUS "Last GIT tag: ${LAST_TAG}")

                execute_process(
                    COMMAND ${GIT_EXECUTABLE} rev-list ${LAST_TAG}.. --count --ancestry-path
                    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
                    OUTPUT_VARIABLE NR_COMMITS
                    OUTPUT_STRIP_TRAILING_WHITESPACE 
                    ERROR_QUIET
                )
                message(STATUS "Commits since last GIT tag: ${NR_COMMITS}")

                string(FIND ${LAST_TAG} "v" pos)
                if (pos MATCHES 0)
                    string(REGEX REPLACE "^v(.*)" "\\1" VERSION_STRING ${LAST_TAG})
                    if("${VERSION_STRING}" MATCHES "^([0-9]+\.[0-9]+\.[0-9])$")
                        # tag has only 3 digits
                        set(STRIPPED "${VERSION_STRING}.")
                        set(TWEAK "0")
                    else()
                        string(REGEX REPLACE "^([0-9]+\\.[0-9]+\\.[0-9]+\\.)([0-9]+)" "\\1" STRIPPED ${VERSION_STRING})
                        string(REGEX REPLACE "^([0-9]+\\.[0-9]+\\.[0-9]+\\.)([0-9]+)" "\\2" TWEAK ${VERSION_STRING})
                    endif()
                else()
                message(STATUS "Tag format incorrect: ${LAST_TAG}")
                    set(STRIPPED "0.0.0.")
                    set(TWEAK "0")
                endif()
            else()
                message(STATUS "No GIT tag found in the history of this branch.")
                set(NR_COMMITS 0)
                set(STRIPPED "0.0.0.")
                set(TWEAK "0")
            endif()

            # detect local changes
            execute_process(
            COMMAND ${GIT_EXECUTABLE} status --untracked-files=no --porcelain
            WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
            OUTPUT_VARIABLE GIT_DIRTY
            ERROR_QUIET
            OUTPUT_STRIP_TRAILING_WHITESPACE
            )
            string(COMPARE EQUAL "${GIT_DIRTY}" "" clean)
            if(clean AND NR_COMMITS MATCHES 0)
                set(FIX_NR ${TWEAK})
            else()
                math(EXPR FIX_NR "10000 + 1000 * ${TWEAK} + ${NR_COMMITS}")
                if(NOT clean)
                    set(GIT_REVISION "${GIT_REVISION} (DIRTY)")
                endif()
            endif()

            string(APPEND STRIPPED ${FIX_NR})
            message(STATUS "Set Version: ${STRIPPED}")
            set(VERSION_STRING "${STRIPPED}")
            message(STATUS "GIT revision: ${GIT_REVISION}")
        else()
            message(STATUS "Not a GIT repository")
            set(GIT_REVISION "NULL")
            set(VERSION_STRING "0.0.0.0")
        endif()
    else()
        message(STATUS "GIT not found")
        set(GIT_REVISION "NULL")
        set(VERSION_STRING "0.0.0.0")
    endif()

    set(${prj_version} "${VERSION_STRING}" PARENT_SCOPE)
    set(${prj_year} "${YEAR}" PARENT_SCOPE)
    set(${prj_timestamp} "${TODAY}" PARENT_SCOPE)
    set(${prj_revision} "${GIT_REVISION}" PARENT_SCOPE)
endfunction()

#
# Applies CMAKE_CXX_FLAGS to all targets in the current CMake directory.
# After this operation, CMAKE_CXX_FLAGS is cleared.
#
macro(apply_global_cxx_flags_to_all_targets)
    separate_arguments(_global_cxx_flags_list UNIX_COMMAND ${CMAKE_CXX_FLAGS})
    get_property(_targets DIRECTORY PROPERTY BUILDSYSTEM_TARGETS)
    foreach(_target ${_targets})
        target_compile_options(${_target} PUBLIC ${_global_cxx_flags_list})
    endforeach()
    unset(CMAKE_CXX_FLAGS)
    set(_flag_sync_required TRUE)
endmacro()

#
# Removes the specified compile flag from the specified target.
#   _target     - The target to remove the compile flag from
#   _flag       - The compile flag to remove
#
# Pre: apply_global_cxx_flags_to_all_targets() must be invoked.
#
macro(remove_flag_from_target _target _flag)
    get_target_property(_target_cxx_flags ${_target} COMPILE_OPTIONS)
    if(_target_cxx_flags)
        list(REMOVE_ITEM _target_cxx_flags ${_flag})
        set_target_properties(${_target} PROPERTIES COMPILE_OPTIONS "${_target_cxx_flags}")
    endif()
endmacro()

#
# Removes the specified compiler flag from the specified file.
#   _target     - The target that _file belongs to
#   _file       - The file to remove the compiler flag from
#   _flag       - The compiler flag to remove.
#
# Pre: apply_global_cxx_flags_to_all_targets() must be invoked.
#
macro(remove_flag_from_file _target _file _flag)
    get_target_property(_target_sources ${_target} SOURCES)
    # Check if a sync is required, in which case we'll force a rewrite of the cache variables.
    if(_flag_sync_required)
        unset(_cached_${_target}_cxx_flags CACHE)
        unset(_cached_${_target}_${_file}_cxx_flags CACHE)
    endif()
    get_target_property(_${_target}_cxx_flags ${_target} COMPILE_OPTIONS)
    # On first entry, cache the target compile flags and apply them to each source file
    # in the target.
    if(NOT _cached_${_target}_cxx_flags)
        # Obtain and cache the target compiler options, then clear them.
        get_target_property(_target_cxx_flags ${_target} COMPILE_OPTIONS)
        set(_cached_${_target}_cxx_flags "${_target_cxx_flags}" CACHE INTERNAL "")
        set_target_properties(${_target} PROPERTIES COMPILE_OPTIONS "")
        # Apply the target compile flags to each source file.
        foreach(_source_file ${_target_sources})
            # Check for pre-existing flags set by set_source_files_properties().
            get_source_file_property(_source_file_cxx_flags ${_source_file} COMPILE_FLAGS)
            if(_source_file_cxx_flags)
                separate_arguments(_source_file_cxx_flags UNIX_COMMAND ${_source_file_cxx_flags})
                list(APPEND _source_file_cxx_flags "${_target_cxx_flags}")
            else()
                set(_source_file_cxx_flags "${_target_cxx_flags}")
            endif()
            # Apply the compile flags to the current source file.
            string(REPLACE ";" " " _source_file_cxx_flags_string "${_source_file_cxx_flags}")
            set_source_files_properties(${_source_file} PROPERTIES COMPILE_FLAGS "${_source_file_cxx_flags_string}")
        endforeach()
    endif()
    list(FIND _target_sources ${_file} _file_found_at)
    if(_file_found_at GREATER -1)
        if(NOT _cached_${_target}_${_file}_cxx_flags)
            # Cache the compile flags for the specified file.
            # This is the list that we'll be removing flags from.
            get_source_file_property(_source_file_cxx_flags ${_file} COMPILE_FLAGS)
            separate_arguments(_source_file_cxx_flags UNIX_COMMAND ${_source_file_cxx_flags})
            set(_cached_${_target}_${_file}_cxx_flags ${_source_file_cxx_flags} CACHE INTERNAL "")
        endif()
        # Remove the specified flag, then re-apply the rest.
        list(REMOVE_ITEM _cached_${_target}_${_file}_cxx_flags ${_flag})
        string(REPLACE ";" " " _cached_${_target}_${_file}_cxx_flags_string "${_cached_${_target}_${_file}_cxx_flags}")
        set_source_files_properties(${_file} PROPERTIES COMPILE_FLAGS "${_cached_${_target}_${_file}_cxx_flags_string}")
    endif()
endmacro()
