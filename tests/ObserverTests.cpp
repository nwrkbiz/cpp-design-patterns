#include "Dg/Patterns/Observer.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class NameHolder : public Dg::Patterns::Observable<NameHolder> {
    std::string m_name;

public:
    NameHolder(std::string name)
        : m_name(std::move(name))
    {
    }
    [[nodiscard]] auto getName() const -> std::string
    {
        return m_name;
    };
};

class MyObserver : public Dg::Patterns::IObserver<NameHolder> {
    std::string m_received;

public:
    auto update(const NameHolder::SPtr& obs) -> void override
    {
        m_received = obs->getName();
    }
    [[nodiscard]] auto getReceived() const -> std::string
    {
        return m_received;
    };
};

TEST(ObserverTests, TestGeneral)
{
    const auto observable = std::make_shared<NameHolder>("Test");

    const auto observer = std::make_shared<MyObserver>();
    observable->subscribe(nullptr);
    observable->subscribe(observer);
    observable->notify();

    EXPECT_EQ("Test", observer->getReceived());
}

TEST(ObserverTests, TestUnsubscribe)
{
    const auto observable = std::make_shared<NameHolder>("Test");

    const auto observer = std::make_shared<MyObserver>();
    observable->subscribe(observer);
    observable->unsubscribe(nullptr);
    observable->unsubscribe(observer);
    observable->notify();

    EXPECT_EQ("", observer->getReceived());
}

TEST(ObserverTests, TestUnsubscribeAll)
{
    const auto observable = std::make_shared<NameHolder>("Test");

    const auto observer = std::make_shared<MyObserver>();
    observable->subscribe(observer);
    observable->subscribe(observer);
    observable->unsubscribeAll();
    observable->notify();

    EXPECT_EQ("", observer->getReceived());
}

} // Dg::Patterns