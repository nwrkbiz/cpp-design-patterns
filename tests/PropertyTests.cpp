#include "Dg/Patterns/Property.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <sstream>
#include <string>

namespace Dg::Patterns {

class ComplexObject : public Object<ComplexObject> {
    size_t m_val = 0;

public:
    ComplexObject(size_t val)
        : m_val(val)
    {
    }

    auto getVal() -> size_t
    {
        return m_val;
    }
};

class PrototypeTestClass : public Object<PrototypeTestClass> {
    size_t m_manualVal { 4 };
    ComplexObject::SPtr m_manualComplexObj { std::make_shared<ComplexObject>(42) };

public:
    Property::Auto<ComplexObject::SPtr> complexObj { nullptr };
    Property::Auto<ComplexObject::SPtr>::ReadOnly complexObjRO { std::make_shared<ComplexObject>(4711) };

    Property::Auto<size_t> valAuto { 1 };
    Property::Auto<size_t>::ReadOnly valAutoRO { 2 };

    Property::Auto<std::string> strVal { "Hello" };

    Property::Manual<size_t> valManual {
        [&]() -> size_t& { return m_manualVal; },
        [&](size_t const& val) { m_manualVal = val; }
    };

    Property::Manual<size_t>::ReadOnly valManualRO {
        [&]() -> size_t& { return m_manualVal; }
    };

    Property::Manual<ComplexObject::SPtr> valManualComplexObj {
        [&]() -> ComplexObject::SPtr& { return m_manualComplexObj; },
        [&](ComplexObject::SPtr const& val) { m_manualComplexObj = val; }
    };

    Property::Manual<ComplexObject::SPtr>::ReadOnly valManualComplexObjRO {
        [&]() -> ComplexObject::SPtr& { return m_manualComplexObj; }
    };
};

TEST(PropertyTest, ManualPropertyReadOnlyTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();

    EXPECT_EQ(4, var->valManualRO);
}

TEST(PropertyTest, ManualComplexObjectTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();
    var->valManualComplexObj = std::make_shared<ComplexObject>(33);

    EXPECT_EQ(33, var->valManualComplexObj->getVal());
}

TEST(PropertyTest, ManualComplexObjectReadOnlyTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();

    EXPECT_EQ(42, var->valManualComplexObjRO->getVal());
}

TEST(PropertyTest, ManualPropertyTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();

    EXPECT_EQ(4, var->valManual);
    var->valManual = 2;
    EXPECT_EQ(2, var->valManual);
}

TEST(PropertyTest, AutoPropertyTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();

    EXPECT_EQ(1, var->valAuto);
    var->valAuto = 2;
    EXPECT_EQ(2, var->valAuto);
}

TEST(PropertyTest, AutoPropertyReadOnlyTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();

    EXPECT_EQ(2, var->valAutoRO);
}

TEST(PropertyTest, AutoComplexObjectTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();
    var->complexObj = std::make_shared<ComplexObject>(3);

    EXPECT_EQ(3, var->complexObj->getVal());
}

TEST(PropertyTest, AutoComplexObjectReadOnlyTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();

    EXPECT_EQ(4711, var->complexObjRO->getVal());
}

TEST(PropertyTest, StringTest)
{
    const auto var = std::make_shared<PrototypeTestClass>();
    std::stringstream sstream;

    sstream << var->strVal;

    EXPECT_STREQ("Hello", sstream.str().c_str());
}

} // Dg::Patterns