#include "Dg/Patterns/Prototype.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class IVarHolder : Dg::Object<IVarHolder> {
public:
    virtual auto getVar() -> std::string = 0;
};

class VarHolder1 : public Dg::Patterns::Prototype<IVarHolder, VarHolder1> {
    std::string m_var;

public:
    VarHolder1(const VarHolder1& origin)
    {
        m_var = origin.m_var + " (clone, 1)";
    }
    VarHolder1(std::string name)
        : m_var(std::move(name)) {};
    auto getVar() -> std::string override
    {
        return m_var;
    }
};

class VarHolder2 : public Dg::Patterns::Prototype<IVarHolder, VarHolder2> {
    std::string m_var;

public:
    VarHolder2(const VarHolder2& origin)
    {
        m_var = origin.m_var + " (clone, 2)";
    }
    VarHolder2(std::string name)
        : m_var(std::move(name)) {};
    auto getVar() -> std::string override
    {
        return m_var;
    }
};

TEST(PrototypeTests, TestCloneShared)
{
    const auto var = std::make_shared<VarHolder1>("Mimi");
    const auto cloneVar = var->cloneShared();

    EXPECT_EQ("Mimi (clone, 1)", cloneVar->getVar());
    EXPECT_EQ("Mimi", var->getVar());
    EXPECT_NE(var.get(), cloneVar.get());
}
TEST(PrototypeTests, TestCloneUnique)
{
    const auto var = std::make_shared<VarHolder2>("Nino");
    const auto cloneVar = var->cloneUnique();

    EXPECT_EQ("Nino (clone, 2)", cloneVar->getVar());
    EXPECT_EQ("Nino", var->getVar());
    EXPECT_NE(var.get(), cloneVar.get());
}

TEST(PrototypeTests, TestCloneRaw)
{
    const auto var = std::make_shared<VarHolder2>("Nino");
    const auto cloneVar = var->cloneRaw();

    EXPECT_EQ("Nino (clone, 2)", cloneVar->getVar());
    EXPECT_EQ("Nino", var->getVar());
    EXPECT_NE(var.get(), cloneVar);

    delete cloneVar;
}

} // Dg::Patterns