#include "Dg/Patterns/Adapter.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class IOrigin : Dg::Object<IOrigin> {
public:
    virtual auto getTrue() -> bool = 0;
};

class IWrongOrigin : Dg::Object<IWrongOrigin> {
public:
    virtual auto getOk() -> bool = 0;
};

// Implementatiom of our class, this is the adaptee
class AdaptMe : public IWrongOrigin {
public:
    auto getOk() -> bool override
    {
        return true;
    }
};

class MyAdapter : public IOrigin, public Dg::Patterns::Adapter<IWrongOrigin> {
public:
    auto getTrue() -> bool override
    {
        return getAdaptee()->getOk();
    }
};

TEST(AdapterTests, Test)
{
    auto adapter = std::make_shared<MyAdapter>();
    adapter->setAdaptee(std::make_shared<AdaptMe>());

    EXPECT_TRUE(adapter->getTrue());
}

} // Dg::Patterns