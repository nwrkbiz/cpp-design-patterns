#include "Dg/Patterns/Mediator.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class MsgReceiver : public Dg::Patterns::Mediatable<const std::string&, MsgReceiver> {
    std::string m_name;

public:
    auto receive(const std::string& name) -> void
    {
        m_name = name;
    }
    [[nodiscard]] auto getName() const -> std::string
    {
        return m_name;
    }
    using SPtr = std::shared_ptr<MsgReceiver>;
};

class Sender : public Dg::Patterns::Mediatable<const std::string&, Sender> {
public:
    auto sendMsg(const std::string& msg) -> void
    {
        getMediator()->mediate(shared_from_this(), msg);
    }
    using SPtr = std::shared_ptr<Sender>;
};

class MyMediator : public Dg::Patterns::IMediator<const std::string&, Sender, MsgReceiver> {
public:
    auto mediate(const MsgReceiver::SPtr& component, const std::string& command) -> void override
    {
        // stub
    }

    auto mediate(const Sender::SPtr& component, const std::string& command) -> void override
    {
        MsgReceiver::SPtr recv = nullptr;
        getMediatable(recv);
        recv->receive(command);
    }
};

TEST(MediatorTests, Test)
{
    const auto mediator = std::make_shared<MyMediator>();

    const auto sender = std::make_shared<Sender>();
    const auto receiver = std::make_shared<MsgReceiver>();

    sender->setMediator(nullptr);
    sender->setMediator(mediator);
    receiver->setMediator(mediator);

    sender->sendMsg("Nino!");

    EXPECT_EQ("Nino!", receiver->getName());
}

} // Dg::Patterns