#include "Dg/Patterns/Builder.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class IToBuild : public Dg::Object<IToBuild> {
public:
    virtual auto getText() -> std::string = 0;
};

class IMyBuilder : public Dg::Object<IMyBuilder> {
public:
    [[nodiscard]] virtual auto getGreeting() const -> std::string = 0;
    [[nodiscard]] virtual auto getName() const -> std::string = 0;
    virtual auto setGreeting(const std::string& greeting) -> std::shared_ptr<Dg::Patterns::IBuilder<IMyBuilder, IToBuild>> = 0;
    virtual auto setName(const std::string& name) -> std::shared_ptr<Dg::Patterns::IBuilder<IMyBuilder, IToBuild>> = 0;
};

// implementation of the immutable class
class ToBuild : public IToBuild {
    std::string m_greeting;
    std::string m_name;

public:
    ToBuild(const Dg::Patterns::IBuilder<IMyBuilder, IToBuild>::SPtr& builder)
        : m_greeting(builder->getGreeting())
        , m_name(builder->getName())
    {
    }
    auto getText() -> std::string override
    {
        return m_greeting + ", " + m_name + "!";
    }
};

// builder class
class MyBuilder : public Dg::Patterns::Builder<IMyBuilder, IToBuild, ToBuild> {
    std::string m_greeting;
    std::string m_name;

public:
    using SPtr = std::shared_ptr<MyBuilder>;

    [[nodiscard]] auto getGreeting() const -> std::string override
    {
        return m_greeting;
    };
    [[nodiscard]] auto getName() const -> std::string override
    {
        return m_name;
    };
    auto setGreeting(const std::string& greeting) -> Dg::Patterns::IBuilder<IMyBuilder, IToBuild>::SPtr override
    {
        m_greeting = greeting;
        return shared_from_this();
    };
    auto setName(const std::string& name) -> Dg::Patterns::IBuilder<IMyBuilder, IToBuild>::SPtr override
    {
        m_name = name;
        return shared_from_this();
    };
};

TEST(BuilderTests, TestBuildUnique)
{
    const auto builder = std::make_shared<MyBuilder>();

    builder->setName("Mimi")->setGreeting("Hello");
    const auto uniqueObj = builder->buildUnique();

    EXPECT_EQ("Hello, Mimi!", uniqueObj->getText());
}

TEST(BuilderTests, TestBuildRaw)
{
    const auto builder = std::make_shared<MyBuilder>();

    builder->setName("Mimi")->setGreeting("Hello");
    const auto rawObj = builder->buildRaw();

    EXPECT_EQ("Hello, Mimi!", rawObj->getText());
    delete rawObj;
}

TEST(BuilderTests, TestBuildShared)
{
    const auto builder = std::make_shared<MyBuilder>();

    builder->setName("Mimi")->setGreeting("Hello");
    const auto sharedObj = builder->buildShared();

    EXPECT_EQ("Hello, Mimi!", sharedObj->getText());
}

} // Dg::Patterns