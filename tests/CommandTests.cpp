#include "Dg/Patterns/Command.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

// interface for our receiver object (-> object receiving commands)
class IReceiver : public Dg::Object<IReceiver> {
public:
    virtual auto addAction() -> void = 0;
    virtual auto undoAction() -> void = 0;
    virtual auto getValue() -> int = 0;
};

// implementation of the receiver object, which shall receive the commands
class Receiver : public IReceiver {
    int m_value = 0;

public:
    auto addAction() -> void override
    {
        m_value++;
    }
    auto undoAction() -> void override
    {
        m_value--;
    }
    auto getValue() -> int override
    {
        return m_value;
    }
};

class IMyCommand : public Dg::Patterns::ICommand<IMyCommand> {
};

// command which executes the receivers functions
class MyCommand : public IMyCommand {
    IReceiver::SPtr m_receiver;

public:
    MyCommand(IReceiver::SPtr receiver)
        : m_receiver(std::move(receiver)) {};

    auto execute() -> void override
    {
        m_receiver->addAction();
    }

    auto undo() -> void override
    {
        m_receiver->undoAction();
    }
};

TEST(CommandTests, TestExecute)
{
    const auto invoker = std::make_shared<Dg::Patterns::Invoker<IMyCommand>>();

    const auto commandReceiver = std::make_shared<Receiver>();
    const auto command = std::make_shared<MyCommand>(commandReceiver);

    invoker->executeCommand(command);

    EXPECT_EQ(1, commandReceiver->getValue());
}

TEST(CommandTests, TestUndo)
{
    const auto invoker = std::make_shared<Dg::Patterns::Invoker<IMyCommand>>();

    const auto commandReceiver = std::make_shared<Receiver>();
    const auto command = std::make_shared<MyCommand>(commandReceiver);

    invoker->executeCommand(command);
    EXPECT_EQ(1, commandReceiver->getValue());

    invoker->undoCommand();
    EXPECT_EQ(0, commandReceiver->getValue());
}

TEST(CommandTests, TestUndoWithoutPop)
{
    const auto invoker = std::make_shared<Dg::Patterns::Invoker<IMyCommand>>();

    const auto commandReceiver = std::make_shared<Receiver>();
    const auto command = std::make_shared<MyCommand>(commandReceiver);

    invoker->undoCommandWithoutPop();

    invoker->executeCommand(command);
    EXPECT_EQ(1, commandReceiver->getValue());

    invoker->undoCommandWithoutPop();
    EXPECT_EQ(0, commandReceiver->getValue());

    invoker->undoCommand();
    EXPECT_EQ(-1, commandReceiver->getValue());
}

TEST(CommandTests, TestPop)
{
    const auto invoker = std::make_shared<Dg::Patterns::Invoker<IMyCommand>>();

    const auto commandReceiver = std::make_shared<Receiver>();
    const auto command = std::make_shared<MyCommand>(commandReceiver);

    invoker->popCommand();

    invoker->executeCommand(command);
    EXPECT_EQ(1, commandReceiver->getValue());

    invoker->popCommand();
    invoker->undoCommand();
    EXPECT_EQ(1, commandReceiver->getValue());
}

TEST(CommandTests, TestClearHistory)
{
    const auto invoker = std::make_shared<Dg::Patterns::Invoker<IMyCommand>>();

    const auto commandReceiver = std::make_shared<Receiver>();
    const auto command = std::make_shared<MyCommand>(commandReceiver);

    invoker->executeCommand(command);
    EXPECT_EQ(1, commandReceiver->getValue());

    invoker->clearHistory();
    invoker->undoCommand();
    EXPECT_EQ(1, commandReceiver->getValue());
}

} // Dg::Patterns