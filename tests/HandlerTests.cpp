#include "Dg/Patterns/HandlerChain.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class IMyHandler : public Dg::Patterns::Handler<IMyHandler, int&> {
};

class Plus : public IMyHandler {
public:
    auto handle(int& val) -> bool override
    {
        val++;
        return true;
    }
};

class Minus : public IMyHandler {
public:
    auto handle(int& val) -> bool override
    {
        val--;
        return true;
    }
};

class Stop : public IMyHandler {
public:
    auto handle(int& val) -> bool override
    {
        return false;
    }
};

TEST(HandlerTests, TestChain)
{
    const auto handler1 = std::make_shared<Plus>();
    const auto handler2 = std::make_shared<Minus>();
    const auto handler3 = std::make_shared<Plus>();

    handler1->setNext(handler2);
    handler2->setNext(handler3);

    int val = 0;
    handler1->startHandling(val);

    EXPECT_EQ(1, val);
}

TEST(HandlerTests, TestStop)
{
    const auto handler1 = std::make_shared<Plus>();
    const auto handler2 = std::make_shared<Plus>();
    const auto handler3 = std::make_shared<Stop>();
    const auto handler4 = std::make_shared<Minus>();

    handler1->setNext(handler2);
    handler2->setNext(handler3);
    handler3->setNext(handler4);

    int val = 0;
    handler1->startHandling(val);

    EXPECT_EQ(2, val);
}

TEST(HandlerTests, TestCreateHandlerChain)
{
    const auto handler1 = IMyHandler::createHandlerChain(std::make_shared<Plus>(), std::make_shared<Plus>(), std::make_shared<Minus>(), std::make_shared<Plus>());

    int val = 0;
    handler1->startHandling(val);

    EXPECT_EQ(2, val);
}

TEST(HandlerTests, TestGetNext)
{
    const auto handler1 = std::make_shared<Plus>();
    const auto handler2 = std::make_shared<Plus>();

    handler1->setNext(handler2);

    EXPECT_EQ(handler2.get(), handler1->getNext().get());
}

} // Dg::Patterns