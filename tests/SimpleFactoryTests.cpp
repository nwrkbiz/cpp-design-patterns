#include "Dg/Patterns/SimpleFactory.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class ISimpleObj : Dg::Object<ISimpleObj> {
public:
    [[nodiscard]] virtual auto getText() const -> std::string = 0;
};

class SimpleObj : public ISimpleObj {
    std::string m_text;

public:
    SimpleObj(std::string name)
        : m_text(std::move(name))
    {
    }
    [[nodiscard]] virtual auto getText() const -> std::string
    {
        return m_text;
    }
};

TEST(SimpleFactoryTests, TestCreateShared)
{
    auto myFactory = std::make_shared<Dg::Patterns::SimpleFactory<std::string, ISimpleObj, const std::string&>>();

    Dg::Patterns::SimpleFactory<std::string, ISimpleObj, const std::string&>::creatorFunctions creator {
        [](const std::string& name) {
            return std::make_shared<SimpleObj>(name);
        },
        [](const std::string& name) {
            return std::make_unique<SimpleObj>(name);
        },
        [](const std::string& name) {
            return new SimpleObj(name);
        }
    };

    myFactory->registerCreator("simpleObj", creator);

    const auto shared = myFactory->createShared("simpleObj", "Mimi");

    EXPECT_EQ("Mimi", shared->getText());
    EXPECT_EQ(nullptr, myFactory->createShared("unknown", "Mimi"));
}

TEST(SimpleFactoryTests, TestCreateUnique)
{
    auto myFactory = std::make_shared<Dg::Patterns::SimpleFactory<std::string, ISimpleObj, const std::string&>>();

    Dg::Patterns::SimpleFactory<std::string, ISimpleObj, const std::string&>::creatorFunctions creator {
        [](const std::string& name) {
            return std::make_shared<SimpleObj>(name);
        },
        [](const std::string& name) {
            return std::make_unique<SimpleObj>(name);
        },
        [](const std::string& name) {
            return new SimpleObj(name);
        }
    };

    myFactory->registerCreator("simpleObj", creator);

    const auto unique = myFactory->createUnique("simpleObj", "Nino");

    EXPECT_EQ("Nino", unique->getText());
    EXPECT_EQ(nullptr, myFactory->createUnique("unknown", "Nino"));
}

TEST(SimpleFactoryTests, TestCreateRaw)
{
    auto myFactory = std::make_shared<Dg::Patterns::SimpleFactory<std::string, ISimpleObj, const std::string&>>();

    Dg::Patterns::SimpleFactory<std::string, ISimpleObj, const std::string&>::creatorFunctions creator {
        [](const std::string& name) {
            return std::make_shared<SimpleObj>(name);
        },
        [](const std::string& name) {
            return std::make_unique<SimpleObj>(name);
        },
        [](const std::string& name) {
            return new SimpleObj(name);
        }
    };

    myFactory->registerCreator("simpleObj", creator);

    const auto rawObject = myFactory->createRaw("simpleObj", "Nino");

    EXPECT_EQ("Nino", rawObject->getText());
    EXPECT_EQ(nullptr, myFactory->createRaw("unknown", "Nino"));
    delete rawObject;
}

} // Dg::Patterns