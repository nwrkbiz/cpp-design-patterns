#include "Dg/Patterns/State.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class MyStateMachine : public Dg::Patterns::StateContext<MyStateMachine, int&> {
};

class EndState : public Dg::Patterns::IState<MyStateMachine, int&> {
    auto executeState(const MyStateMachine::SPtr& /* ctx */, int& /* val */) -> void override
    {
    }
};

class SubState : public Dg::Patterns::IState<MyStateMachine, int&> {
    auto executeState(const MyStateMachine::SPtr& ctx, int& val) -> void override
    {
        val--;
        ctx->setState(std::make_shared<EndState>());
    }
};

class Add2State : public Dg::Patterns::IState<MyStateMachine, int&> {
public:
    auto executeState(const MyStateMachine::SPtr& ctx, int& val) -> void override
    {
        val++;
        val++;
        ctx->setState(std::make_shared<SubState>());
    }
};

TEST(StateTest, TestStateMachine)
{
    int myVal = 1;

    const auto stateMachine = std::make_shared<MyStateMachine>();
    stateMachine->setState(std::make_shared<Add2State>());

    stateMachine->executeState(myVal);
    stateMachine->executeState(myVal);
    stateMachine->executeState(myVal);
    stateMachine->executeState(myVal);
    EXPECT_EQ(2, myVal);
}

TEST(StateTest, TestGetState)
{
    int myVal = 1;

    const auto stateMachine = std::make_shared<MyStateMachine>();
    stateMachine->setState(std::make_shared<Add2State>());

    stateMachine->executeState(myVal);

    int myTestVal = 1;
    stateMachine->getState()->executeState(stateMachine, myTestVal);

    EXPECT_EQ(0, myTestVal);
}

} // Dg::Patterns