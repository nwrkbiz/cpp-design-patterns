#include "Dg/Patterns/PassKey.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class MyKeyholderClass : public Dg::Object<MyKeyholderClass> {
    Dg::Patterns::Key<MyKeyholderClass> m_key;

public:
    virtual auto calculate() -> void final
    {
        calculateImpl(m_key);
    }

protected:
    virtual void calculateImpl(const Dg::Patterns::Key<MyKeyholderClass>& /*key*/) = 0;
};

class MyLockedClass : public MyKeyholderClass {
    int m_result = 0;

public:
    [[nodiscard]] auto getResult() const -> int
    {
        return m_result;
    }

protected:
    auto calculateImpl(const Dg::Patterns::Key<MyKeyholderClass>& /*key*/) -> void override
    {
        m_result = 2 + 3;
    }
};

TEST(PassKeyTests, Test)
{
    const auto calc = std::make_unique<MyLockedClass>();
    EXPECT_EQ(0, calc->getResult());

    calc->calculate();

    EXPECT_EQ(5, calc->getResult());
}

} // Dg::Patterns