#include "Dg/Patterns/Singleton.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class MySingleton : public Singleton<MySingleton> {
    friend class Singleton<MySingleton>;
    std::string m_name;
    MySingleton() = default;
    MySingleton(std::string name)
        : m_name(std::move(name)) {};

public:
    auto getName() -> std::string
    {
        return m_name;
    }
};

TEST(SingletonTests, getInstanceBasic)
{
    std::string name1 = MySingleton::getInstance("Nino")->getName();
    std::string name2 = MySingleton::getInstance()->getName();
    EXPECT_EQ("Nino", name1);
    EXPECT_EQ("Nino", name2);
}

TEST(SingletonTests, getInstanceAreEqual)
{
    auto* inst1 = MySingleton::getInstance("Nino");
    auto* inst2 = MySingleton::getInstance();
    EXPECT_EQ(inst1, inst2);
}

TEST(SingletonTests, destroy)
{
    std::string name1 = MySingleton::getInstance("Nino")->getName();
    MySingleton::destroy();
    std::string name2 = MySingleton::getInstance()->getName();
    EXPECT_NE(name1, name2);
    EXPECT_EQ("Nino", name1);
    EXPECT_EQ("", name2);
}

} // Dg::Patterns