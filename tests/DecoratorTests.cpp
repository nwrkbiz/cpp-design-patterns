#include "Dg/Patterns/Decorator.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class IDecoratee : Dg::Object<IDecoratee> {
public:
    virtual auto getText() -> std::string = 0;
};

class Decoratee : public IDecoratee {
public:
    auto getText() -> std::string override
    {
        return "Hi";
    }
};

class MyDecorator : public Dg::Patterns::Decorator<IDecoratee> {
public:
    auto getText() -> std::string override
    {
        return getDecoratee()->getText();
    }
};

class DecorateNino : public MyDecorator {
public:
    auto getText() -> std::string override
    {
        return MyDecorator::getText() + ", Nino";
    }
};

// another concrete decorator
class DecorateMimi : public MyDecorator {
public:
    auto getText() -> std::string override
    {
        return MyDecorator::getText() + ", Mimi";
    }
};

TEST(DecoratorTests, Test)
{
    const auto decoratee = std::make_shared<Decoratee>();

    const auto decorateNino = std::make_shared<DecorateNino>();
    decorateNino->setDecoratee(decoratee);

    const auto decorateMimi = std::make_shared<DecorateMimi>();
    decorateMimi->setDecoratee(decoratee);

    const auto decorateNino2 = std::make_shared<DecorateNino>();
    decorateNino2->setDecoratee(decorateMimi);

    EXPECT_EQ("Hi, Mimi", decorateMimi->getText());
    EXPECT_EQ("Hi, Nino", decorateNino->getText());
    EXPECT_EQ("Hi, Mimi, Nino", decorateNino2->getText());
}

} // Dg::Patterns