#include "Dg/Patterns/FactoryMethod.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class IObj : Dg::Object<IObj> {
public:
    virtual auto getVal() -> int = 0;
};

class Obj : public IObj {
    int m_val;

public:
    using Factory = Dg::Patterns::FactoryMethod<IObj, Obj, int>;
    using IFactory = Dg::Patterns::IFactoryMethod<IObj, int>;
    Obj(int val)
        : m_val(val)
    {
    }
    auto getVal() -> int override
    {
        return m_val;
    }
};

TEST(FactoryMethodTests, TestCreateShared)
{
    const auto myFactory = std::make_shared<Obj::Factory>();

    const auto myObj = myFactory->createShared(1);
    EXPECT_EQ(1, myObj->getVal());
}

TEST(FactoryMethodTests, TestCreateUnique)
{
    const auto myFactory = std::make_shared<Obj::Factory>();

    const auto myObj = myFactory->createUnique(2);
    EXPECT_EQ(2, myObj->getVal());
}

TEST(FactoryMethodTests, TestCreateRaw)
{
    const auto myFactory = std::make_shared<Obj::Factory>();

    const auto myObj = myFactory->createRaw(2);
    EXPECT_EQ(2, myObj->getVal());
    delete myObj;
}

} // Dg::Patterns