#include "Dg/Patterns/Memento.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class Editor : public Dg::Patterns::IOriginator<Editor, const std::string&> {
    std::string m_text;

    class EditorData : public Dg::Patterns::Memento<Editor, const std::string&> {
        friend Editor;
        std::string m_text;
        auto restore(const std::string& param) -> void override
        {
            getOriginator()->m_text = m_text + " " + param;
        };
    };

public:
    auto createMemento() -> Dg::Patterns::IMemento<Editor, const std::string&>::SPtr override
    {
        auto snapshot = std::make_shared<EditorData>();
        snapshot->setOriginator(nullptr); // error case, should be ignored
        snapshot->setOriginator(shared_from_this());
        snapshot->m_text = m_text;
        return snapshot;
    }

    auto setText(const std::string& txt) -> void
    {
        m_text = txt;
    }

    [[nodiscard]] auto getText() const -> std::string
    {
        return m_text;
    };
};

TEST(MementoTests, Test)
{
    const auto editor = std::make_shared<Editor>();
    editor->setText("Hello, Mimi!");
    EXPECT_EQ("Hello, Mimi!", editor->getText());

    const auto snapshot = editor->createMemento();
    editor->setText("Hello, Nino!");
    EXPECT_EQ("Hello, Nino!", editor->getText());

    snapshot->restore("...restored");
    EXPECT_EQ("Hello, Mimi! ...restored", editor->getText());
}

} // Dg::Patterns