#include "Dg/Patterns/Visitor.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace Dg::Patterns {

class VisitableA : public Dg::Patterns::Visitable<VisitableA> {
    bool m_visitted = false;

public:
    auto visitA() -> void
    {
        m_visitted = true;
    }

    [[nodiscard]] auto getVisitted() const -> bool
    {
        return m_visitted;
    }
};

class VisitableB : public Dg::Patterns::Visitable<VisitableB> {
    bool m_visitted = false;

public:
    auto visitB() -> void
    {
        m_visitted = true;
    }

    [[nodiscard]] auto getVisitted() const -> bool
    {
        return m_visitted;
    }
};

class MyVisitor : public Dg::Patterns::IVisitor<VisitableA, VisitableB> {
public:
    // Visit function for SayHi objects
    auto visit(const std::shared_ptr<VisitableA>& elem) -> void override
    {
        elem->visitA();
    }

    // Visit function for SayHello objects
    auto visit(const std::shared_ptr<VisitableB>& elem) -> void override
    {
        elem->visitB();
    }
};

TEST(VisitorTests, Test)
{
    const auto visitor = std::make_shared<MyVisitor>();

    // create our visitable objects
    auto visitableA = std::make_shared<VisitableA>();
    auto visitableB = std::make_shared<VisitableB>();
    EXPECT_FALSE(visitableA->getVisitted());
    EXPECT_FALSE(visitableB->getVisitted());

    visitableA->doVisit(visitor);
    visitableB->doVisit(visitor);
    EXPECT_TRUE(visitableA->getVisitted());
    EXPECT_TRUE(visitableB->getVisitted());
}

} // Dg::Patterns