#include "Dg/Patterns/Composite.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace Dg::Patterns {

class INode : public Dg::Patterns::Composite<INode> {
public:
    virtual auto getValue() -> int = 0;
};

class Node : public INode {
    int m_value = {};

public:
    Node(int val)
        : m_value(val)
    {
    }

    auto getValue() -> int override
    {
        return m_value;
    }
};

class INodePostOrder : public Dg::Patterns::Composite<INodePostOrder, Patterns::Iterators::Composite::PostOrderIterator<INodePostOrder>> {
public:
    virtual auto getValue() -> int = 0;
};

class NodePostOrder : public INodePostOrder {
    int m_value = {};

public:
    NodePostOrder(int val)
        : m_value(val)
    {
    }

    auto getValue() -> int override
    {
        return m_value;
    }
};

TEST(CompositeTests, GetChildTest)
{
    auto rootElem = std::make_shared<Node>(0);
    auto rootChild = std::make_shared<Node>(1);
    auto rootChildChild = std::make_shared<Node>(2);

    rootElem->addChild(rootChild);
    rootChild->addChild(rootChildChild);

    EXPECT_EQ(0, rootElem->getValue());
    EXPECT_EQ(1, rootElem->getChildren().at(0)->getValue());
    EXPECT_EQ(2, rootElem->getChildren().at(0)->getChildren().at(0)->getValue());
    EXPECT_EQ(false, rootElem->isLeaf());
    EXPECT_EQ(true, rootElem->getChildren().at(0)->getChildren().at(0)->isLeaf());
}

TEST(CompositeTests, GetParentTest)
{
    auto rootElem = std::make_shared<Node>(0);
    auto rootChild = std::make_shared<Node>(1);
    auto rootChildChild = std::make_shared<Node>(2);

    rootElem->addChild(rootChild);
    rootChild->addChild(rootChildChild);

    EXPECT_EQ(2, rootChildChild->getValue());
    EXPECT_EQ(1, rootChildChild->getParent()->getValue());
    EXPECT_EQ(0, rootChildChild->getParent()->getParent()->getValue());
}

TEST(CompositeTests, SetParentTest)
{
    auto rootElem = std::make_shared<Node>(0);
    auto rootChild = std::make_shared<Node>(1);
    auto rootChildChild = std::make_shared<Node>(2);

    rootElem->addChild(rootChild);
    rootChildChild->setParent(rootChild);

    EXPECT_EQ(2, rootChildChild->getValue());
    EXPECT_EQ(1, rootChildChild->getParent()->getValue());
    EXPECT_EQ(0, rootChildChild->getParent()->getParent()->getValue());
}

TEST(CompositeTests, GetNumberOfChildren)
{
    auto rootElem = std::make_shared<Node>(0);
    auto rootChild1 = std::make_shared<Node>(1);
    auto rootChild2 = std::make_shared<Node>(2);

    rootElem->addChild(rootChild1);
    rootElem->addChild(rootChild2);

    EXPECT_EQ(2, rootElem->getChildren().size());
}

TEST(CompositeTests, RemoveChild)
{
    auto rootElem = std::make_shared<Node>(0);
    auto rootChild1 = std::make_shared<Node>(1);
    auto rootChild2 = std::make_shared<Node>(2);

    rootElem->addChild(rootChild1);
    rootElem->addChild(rootChild2);

    rootElem->removeChild(nullptr);
    rootElem->removeChild(rootChild1);

    EXPECT_EQ(1, rootElem->getChildren().size());
    EXPECT_EQ(2, rootElem->getChildren().at(0)->getValue());
}

TEST(CompositeTests, PreOrderIteratorTest)
{
    auto rootElem = std::make_shared<Node>(0);
    auto rootChild1 = std::make_shared<Node>(1);
    auto rootChild2 = std::make_shared<Node>(2);
    auto rootChild3 = std::make_shared<Node>(3);

    auto rootChild11 = std::make_shared<Node>(11);
    auto rootChild12 = std::make_shared<Node>(12);

    auto rootChild21 = std::make_shared<Node>(21);
    auto rootChild22 = std::make_shared<Node>(22);

    auto rootChild31 = std::make_shared<Node>(31);
    auto rootChild32 = std::make_shared<Node>(32);

    rootElem->addChild(rootChild1);
    rootChild1->addChild(rootChild11);
    rootChild1->addChild(rootChild12);

    rootElem->addChild(rootChild2);
    rootChild2->addChild(rootChild21);
    rootChild2->addChild(rootChild22);

    rootElem->addChild(rootChild3);
    rootChild3->addChild(rootChild31);
    rootChild3->addChild(rootChild32);

    std::vector<int> vals;

    for (const auto& elem : *rootElem) {
        vals.emplace_back(elem->getValue());
    }

    std::vector<int> expected { 0, 1, 11, 12, 2, 21, 22, 3, 31, 32 };
    EXPECT_EQ(expected, vals);
}

TEST(CompositeTests, PostOrderIteratorTest)
{
    auto rootElem = std::make_shared<NodePostOrder>(0);
    auto rootChild1 = std::make_shared<NodePostOrder>(1);
    auto rootChild2 = std::make_shared<NodePostOrder>(2);
    auto rootChild3 = std::make_shared<NodePostOrder>(3);

    auto rootChild11 = std::make_shared<NodePostOrder>(11);
    auto rootChild12 = std::make_shared<NodePostOrder>(12);

    auto rootChild21 = std::make_shared<NodePostOrder>(21);
    auto rootChild22 = std::make_shared<NodePostOrder>(22);

    auto rootChild31 = std::make_shared<NodePostOrder>(31);
    auto rootChild32 = std::make_shared<NodePostOrder>(32);

    rootElem->addChild(rootChild1);
    rootChild1->addChild(rootChild11);
    rootChild1->addChild(rootChild12);

    rootElem->addChild(rootChild2);
    rootChild2->addChild(rootChild21);
    rootChild2->addChild(rootChild22);

    rootElem->addChild(rootChild3);
    rootChild3->addChild(rootChild31);
    rootChild3->addChild(rootChild32);

    std::vector<int> vals;

    for (const auto& elem : *rootElem) {
        vals.emplace_back(elem->getValue());
    }

    std::vector<int> expected { 0, 3, 32, 31, 2, 22, 21, 1, 12, 11 };
    EXPECT_EQ(expected, vals);
}

} // Dg::Patterns