/**
 * @file Command.h
 * @brief Command pattern template classes.
 *
 * The command pattern is a behavioral pattern that uses an invoker class to execute callbacks that are encapsulated into objects. Because
 * a custom invoker is used, auditing, undo functionalities and similar things can easily be implemented.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * To simplify the diagram, the interface class for Invoker (IInvoker) has been omitted.
 *
 * @image html Command.drawio.svg
 *
 * ### Code
 *
 * @include Command.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_COMMAND_H
#define DG_DESIGNPATTERNS_COMMAND_H

#include "Dg/Object.h"

#include <stack>

namespace Dg::Patterns {

/**
 * @brief Interface for the Command Object, inherit from this class to create your commands.
 * In most cases it makes sense to create an intermediate command interface class from which you
 * inherit all your concrete commands.
 * @tparam I Class/Interface name of the inheriting class.
 * @tparam Args List of argument types which may be passed to the execute and undo functions.
 */
template <typename I, typename... Args>
class ICommand : public Object<I> {
public:
    /**
     * @brief Execute the command.
     * @param args List of (optional) arguments to pass to the execute function.
     */
    virtual auto execute(Args... args) -> void = 0;

    /**
     * @brief Undo the command.
     * @param args List of (optional) arguments to pass to undo function.
     */
    virtual auto undo(Args... args) -> void = 0;
};

/**
 * @brief Interface for the Invoker Object. Can be used e.g. for mocking purposes.
 * @tparam T Class/Interface name of the command (interface-)class.
 * @tparam Args List of argument types which may be passed to the commands execute and undo functions.
 */
template <typename T, typename... Args>
class IInvoker : public Object<IInvoker<T, Args...>> {
public:
    /**
     * @brief Invokes a given command and pushes it to the stack.
     * @param command Command to invoke.
     * @param args Arguments to pass to the execute function.
     */
    virtual auto executeCommand(const std::shared_ptr<ICommand<T, Args...>>& command, Args... args) -> void = 0;

    /**
     * @brief Invokes the undo command of the stacks top element and pops it from the stack.
     * @param args Arguments to pass to the undo function.
     */
    virtual auto undoCommand(Args... args) -> void = 0;

    /**
     * @brief Invokes the undo command of the stacks top element without popping it from the stack.
     * @param args Arguments to pass to the undo function.
     */
    virtual auto undoCommandWithoutPop(Args... args) -> void = 0;

    /**
     * @brief Pops a command from the stack without executing it.
     */
    virtual auto popCommand() -> void = 0;

    /**
     * @brief Clears the complete command stack.
     */
    virtual auto clearHistory() -> void = 0;
};

/**
 * @brief Implementation of the invoker. The invoker maintains a stack of executed commands, which it utilizes
 * for the undo functionality. This is a commonly used invoker, you may however need to implement a own one
 * that fits your purpose better.
 * @tparam T Class/Interface name of the command (interface-)class.
 * @tparam Args List of argument types which may be passed to the commands execute and undo functions.
 */
template <typename T, typename... Args>
class Invoker : public IInvoker<T, Args...> {
    std::stack<std::shared_ptr<ICommand<T, Args...>>> m_history;

protected:
    /**
     * @return The stack object containing the command history.
     */
    auto getStack() -> std::stack<std::shared_ptr<ICommand<T, Args...>>>&
    {
        return m_history;
    }

public:
    auto executeCommand(const std::shared_ptr<ICommand<T, Args...>>& command, Args... args) -> void override
    {
        command->execute(std::forward<Args>(args)...);
        m_history.push(command);
    }

    auto undoCommand(Args... args) -> void override
    {
        if (m_history.empty()) {
            return;
        }

        m_history.top()->undo(std::forward<Args>(args)...);
        m_history.pop();
    }

    auto undoCommandWithoutPop(Args... args) -> void override
    {
        if (m_history.empty()) {
            return;
        }

        m_history.top()->undo(std::forward<Args>(args)...);
    }

    auto popCommand() -> void override
    {
        if (m_history.empty()) {
            return;
        }
        m_history.pop();
    }

    auto clearHistory() -> void override
    {
        m_history = {};
    } /**
       * @brief Convenience definition for a shared pointer.
       */
    using SPtr = std::shared_ptr<Invoker<T, Args...>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<Invoker<T, Args...>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<Invoker<T, Args...>>;
};
} // namespace Dg::Patterns
#endif // DG_DESIGNPATTERNS_COMMAND_H