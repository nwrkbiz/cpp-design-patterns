/**
 * @file SimpleFactory.h
 * @brief SimpleFactory pattern template classes.
 *
 * The simple factory pattern is a creational design pattern that allows creating different concrete objects of one interface using
 * custom keys. Useful if the evaluation of which concrete object to create depends on runtime information.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html SimpleFactory.drawio.svg
 *
 * ### Code
 *
 * @include SimpleFactory.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_SIMPLE_FACTORY_H
#define DG_DESIGNPATTERNS_SIMPLE_FACTORY_H

#include "Dg/Object.h"

#include <functional>
#include <map>
#include <tuple>

namespace Dg::Patterns {

/**
 * @brief Interface class for the SimpleFactory class. Can be used e.g. for mocking purposes.
 * @tparam KeyType Type of the element which is used to distinguish between the different concrete types.
 * @tparam I Interface/Base class of the concrete types.
 * @tparam Args List of argument types which may be passed to the concrete elements constructor.
 */
template <typename KeyType, typename I, typename... Args>
class ISimpleFactory : public Object<ISimpleFactory<KeyType, I, Args...>> { // NOLINT, clang-tidy bug
public:
    /**
     * @brief Definition for a tuple of functions which are used to create the concrete objects.
     */
    using creatorFunctions = std::tuple<std::function<std::shared_ptr<I>(Args...)>, std::function<std::unique_ptr<I>(Args...)>, std::function<I*(Args...)>>;
    ~ISimpleFactory() override = default;

    /**
     * @brief Allows registering new creator functions for concrete objects.
     * @param key Key which can later be used to construct the object returned by the given creator.
     * @param creators Creator functions which can create a shared and a unique pointer of a concrete object.
     */
    virtual auto registerCreator(const KeyType& key, const creatorFunctions& creators) -> void = 0;

    /**
     * @brief Constructs a shared pointer of a concrete object.
     * @param key Key of the creator which shall be used.
     * @param args Arguments which are needed to construct the object.
     * @return Shared pointer of the concrete object.
     */
    virtual auto createShared(const KeyType& key, Args... args) -> std::shared_ptr<I> = 0;

    /**
     * @brief Constructs a unique pointer of a concrete object.
     * @param key Key of the creator which shall be used.
     * @param args Arguments which are needed to construct the object.
     * @return Unique pointer of the concrete object.
     */
    virtual auto createUnique(const KeyType& key, Args... args) -> std::unique_ptr<I> = 0;

    /**
     * @brief Constructs a raw pointer of a concrete object, yields ownership.
     * @param key Key of the creator which shall be used.
     * @param args Arguments which are needed to construct the object.
     * @return Raw pointer of the concrete object.
     */
    [[nodiscard]] virtual auto createRaw(const KeyType& key, Args... args) -> I* = 0;
};

/**
 * @brief SimpleFactory class, which provides an abstraction to the creation of concrete objects which
 * are defined by an interface.
 * @tparam KeyType Type of the element which is used to distinguish between the different concrete types.
 * @tparam I Interface/Base class of the concrete types.
 * @tparam Args List of argument types which may be passed to the concrete elements constructor.
 */
template <typename KeyType, typename I, typename... Args>
class SimpleFactory : public ISimpleFactory<KeyType, I, Args...> {
public:
    /**
     * @brief Definition for a tuple of functions which are used to create the concrete objects.
     */
    using creatorFunctions = std::tuple<std::function<std::shared_ptr<I>(Args...)>, std::function<std::unique_ptr<I>(Args...)>, std::function<I*(Args...)>>;

    auto registerCreator(const KeyType& key, const creatorFunctions& creators) -> void override
    {
        m_creators[key] = creators;
    }

    auto createShared(const KeyType& key, Args... args) -> std::shared_ptr<I> override
    {
        const auto creator = m_creators.find(key);
        if (creator == m_creators.end()) {
            return nullptr;
        }

        return std::get<0>(creator->second)(std::forward<Args>(args)...);
    };

    auto createUnique(const KeyType& key, Args... args) -> std::unique_ptr<I> override
    {
        const auto creator = m_creators.find(key);
        if (creator == m_creators.end()) {
            return nullptr;
        }

        return std::get<1>(creator->second)(std::forward<Args>(args)...);
    };

    [[nodiscard]] auto createRaw(const KeyType& key, Args... args) -> I* override
    {
        const auto creator = m_creators.find(key);
        if (creator == m_creators.end()) {
            return nullptr;
        }

        return std::get<2>(creator->second)(std::forward<Args>(args)...);
    };

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<SimpleFactory<KeyType, I, Args...>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<SimpleFactory<KeyType, I, Args...>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<SimpleFactory<KeyType, I, Args...>>;

private:
    std::map<KeyType, creatorFunctions> m_creators;
};
} // Dg::Patterns
#endif // DG_DESIGNPATTERNS_SIMPLE_FACTORY_H