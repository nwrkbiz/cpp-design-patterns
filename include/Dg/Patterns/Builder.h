/**
 * @file Builder.h
 * @brief Builder pattern template classes.
 *
 * The builder pattern is a creational design pattern that is usually used to (optionally) set one or multiple members of an immutable class.
 * This can be used among other things to prevent the telescoping constructor "antipattern" (multiple constructor overloads which allow setting multiple combinations of member variables).
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html Builder.drawio.svg
 *
 * ### Code
 *
 * @include Builder.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_BUILDER_H
#define DG_DESIGNPATTERNS_BUILDER_H

#include "Dg/Object.h"

namespace Dg::Patterns {

/**
 * @brief Interface class for the Builder class. Can be used e.g. for mocking purposes.
 * Inherit your builder Interface from this interface class.
 * @tparam B Name of the inheriting builder interface class.
 * @tparam I Interface/Base class of the object to build.
 */
template <typename B, typename I>
class IBuilder : public B {
public:
    /**
     * @brief Build the object and return a shared pointer.
     * @return Shared pointer to the built object.
     */
    virtual auto buildShared() -> std::shared_ptr<I> = 0;

    /**
     * @brief Build the object and return a unique pointer.
     * @return Unique pointer to the built object.
     */
    virtual auto buildUnique() -> std::unique_ptr<I> = 0;

    /**
     * @brief Build the object and return a raw pointer, yields ownership.
     * @return Raw pointer to the built object.
     */
    [[nodiscard]] virtual auto buildRaw() -> I* = 0;

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<IBuilder<B, I>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<IBuilder<B, I>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<IBuilder<B, I>>;
};

/**
 * @brief Implementation of the Builder template class.
 * Inherit from this class to implement your concrete builder. The class
 * to build needs to accept a shared/weak pointer of this builder as CTor
 * argument.
 * @tparam B Name of the builder interface class.
 * @tparam I Interface/Base class of the object to build.
 * @tparam T Concrete class to build.
 */
template <typename B, typename I, typename T>
class Builder : public IBuilder<B, I>, public std::enable_shared_from_this<Builder<B, I, T>> { // NOLINT
protected:
    /**
     * @brief Constructor
     */
    Builder() = default;
    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Builder(const Builder& origin) = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Builder(Builder&& origin) noexcept = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(const Builder& origin) -> Builder& = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(Builder&& origin) noexcept -> Builder& = default;

public:
    auto buildShared() -> std::shared_ptr<I> override
    {
        return std::make_shared<T>(this->shared_from_this());
    };

    auto buildUnique() -> std::unique_ptr<I> override
    {
        return std::make_unique<T>(this->shared_from_this());
    };

    [[nodiscard]] auto buildRaw() -> I* override
    {
        return new T(this->shared_from_this()); // NOLINT
    };

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<Builder<B, I, T>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<Builder<B, I, T>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<Builder<B, I, T>>;
};
} // Dg::Patterns
#endif // DG_DESIGNPATTERNS_BUILDER_H