/**
 * @file Visitor.h
 * @brief Visitor pattern template classes.
 *
 * The visitor pattern is a behavioral pattern that can be used to separate algorithms from objects they operate on.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html Visitor.drawio.svg
 *
 * ### Code
 *
 * @include Visitor.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_VISITOR_H
#define DG_DESIGNPATTERNS_VISITOR_H

#include "Dg/Object.h"

#include <algorithm>
#include <vector>

namespace Dg::Patterns {

/**
 * @brief (Internal) helper class, provides a visitor function for one concrete type.
 * IVisitor inherits from this for every concrete type. Not intended to be used directly.
 * @tparam T concrete class to provide a visitor function for.
 * @tparam R (Optional) return type of the visit function, defaults to void.
 */
template <typename T, typename R = void>
class ILonelyVisitor : public Object<ILonelyVisitor<T>> {
public:
    /**
     * @brief A visitable object will call this function when its doVisit function is called.
     * @param elem The concrete visitable object.
     */
    virtual auto visit(const std::shared_ptr<T>& elem) -> R = 0;
};

/**
 * @brief Interface class for the visitor to create. Inherit from this class to implement your
 * concrete visitor.
 * @tparam Ts List of types, which the concrete visitor shall be able to visit.
 */
template <typename... Ts>
class IVisitor : public ILonelyVisitor<Ts>... {
public:
    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<IVisitor<Ts...>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<IVisitor<Ts...>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<IVisitor<Ts...>>;
};

/**
 * @brief Interface for the visitable class, can be used e.g. for mocking purposes.
 * @tparam T Name of the (inheriting) visitable class.
 */
template <typename T>
class IVisitable : public Object<IVisitable<T>> {
public:
    /**
     * @brief Allows the given visitor class to execute its visit function for the inherited class.
     * @param visitor Visitor which should execute its visit function for the object which inherited from this class.
     */
    virtual auto doVisit(const std::weak_ptr<ILonelyVisitor<T>>& visitor) -> void = 0;
};

/**
 * @brief Base class for your objects which shall be visitable by your concrete visitor.
 * Your visitable classes shall inherit from this class to get the doVisit function.
 * @tparam T Name of the (inheriting) visitable class.
 */
template <typename T>
class Visitable : public IVisitable<T>, public std::enable_shared_from_this<T> { // NOLINT, clang-tidy bug
public:
    auto doVisit(const std::weak_ptr<ILonelyVisitor<T>>& visitor) -> void override
    {
        visitor.lock()->visit(this->shared_from_this());
    }

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<T>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<T>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<T>;
};
} // namespace Dg::Patterns
#endif // DG_DESIGNPATTERNS_VISITOR_H