/**
 * @file Singleton.h
 * @brief Singleton pattern template classes.
 *
 * The singleton pattern is a creational design pattern that ensures, that a class only has one object instance.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html Singleton.drawio.svg
 *
 * ### Code
 *
 * @include Singleton.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_SINGLETON_H
#define DG_DESIGNPATTERNS_SINGLETON_H

#include "Dg/Object.h"

#include <mutex>

namespace Dg::Patterns {

/**
 * @brief Singleton Template Class. Inherit from this class to make your class
 * a singleton. You need to mark this class as friend class within your inheriting class.
 * @tparam T Interface/Base class of the class which shall become a singleton.
 */
template <typename T>
class Singleton : public Object<T> { // NOLINT
    inline static std::unique_ptr<T> s_instance; // NOLINT
    inline static std::mutex s_mutex; // NOLINT

public:
    /**
     * @brief Generates a static Variable which can only be instanced once.
     * Parameters depend on the inheriting classes constructor(s).
     * @param args Arguments that shall be passed to the classes constructor.
     * @return Pointer to the only existing instance.
     */
    template <typename... Args>
    static auto getInstance(Args... args) -> T*
    {
        if (s_instance == nullptr) {
            std::lock_guard<std::mutex> lock(s_mutex);
            if (s_instance == nullptr) {
                s_instance.reset(new T { std::forward<Args>(args)... }); // NOLINT
            }
        }
        return s_instance.get();
    }

    /**
     * @brief Destroys the Object held by the Singleton.
     */
    static auto destroy() -> void
    {
        s_instance = nullptr;
    }

    /**
     * @brief Destructor
     */
    ~Singleton() override = default;

    Singleton(const Singleton&) = delete;
    Singleton(Singleton&&) noexcept = delete;
    auto operator=(const Singleton&) -> Singleton& = delete;
    auto operator=(Singleton&&) noexcept -> Singleton& = delete;

protected:
    // No Object of Singleton can be created
    Singleton() = default;
};
} // Dg::Patterns
#endif // DG_DESIGNPATTERNS_SINGLETON_H