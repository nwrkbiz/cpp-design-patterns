/**
 * @file Adapter.h
 * @brief Adapter pattern template classes.
 *
 * The adapter pattern is a structural design pattern that can be used to adapt one interface/class to another interface.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html Adapter.drawio.svg
 *
 * ### Code
 *
 * @include Adapter.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_ADAPTER_H
#define DG_DESIGNPATTERNS_ADAPTER_H

#include "Dg/Object.h"

namespace Dg::Patterns {

/**
 * @brief Interface class for the Adapter class. Can be used e.g. for mocking purposes.
 * @tparam T Interface/Base class of the adaptee.
 */
template <typename T>
class IAdapter : public Object<IAdapter<T>> {
public:
    /**
     * @brief Sets the class to be adapted.
     * @param adaptee Class to be adapted.
     */
    virtual auto setAdaptee(const std::shared_ptr<T>& adaptee) -> void = 0;

    /**
     * @return The class which is adapted by this adapter.
     */
    [[nodiscard]] virtual auto getAdaptee() const -> std::shared_ptr<T> = 0;
};

/**
 * @brief Implementation of the Adapter template class.
 * Inherit from this class to implement your own adapter.
 * Within the adapter you need to forward the adapted functions
 * to the adaptee.
 * @tparam T Interface/Base class of the adaptee.
 */
template <typename T>
class Adapter : public IAdapter<T> { // NOLINT
    std::shared_ptr<T> m_adaptee;

protected:
    /**
     * @brief Constructor
     */
    Adapter() = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Adapter(const Adapter& origin) = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Adapter(Adapter&& origin) noexcept = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(const Adapter& origin) -> Adapter& = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(Adapter&& origin) noexcept -> Adapter& = default;

public:
    /**
     * @brief Destructor
     */
    ~Adapter() override = default;

    auto setAdaptee(const std::shared_ptr<T>& adaptee) -> void override
    {
        m_adaptee = adaptee;
    }

    [[nodiscard]] auto getAdaptee() const -> std::shared_ptr<T> override
    {
        return m_adaptee;
    }

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<Adapter<T>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<Adapter<T>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<Adapter<T>>;
};
} // Dg::Patterns
#endif // DG_DESIGNPATTERNS_ADAPTER_H