/**
 * @file Memento.h
 * @brief Memento pattern template classes.
 *
 * The memento pattern is a behavioral pattern that can be used to save and restore the state of an object without
 * revealing any implementation details.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html Memento.drawio.svg
 *
 * ### Code
 *
 * @include Memento.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_MEMENTO_H
#define DG_DESIGNPATTERNS_MEMENTO_H

#include "Dg/Object.h"

namespace Dg::Patterns {

/**
 * @brief Interface for an memmento object. (can be used for mocking purposes)
 * @tparam T Class which we shall be able to create an memento for.
 * @tparam Args List of types which shall be passable to the restore function.
 */
template <typename T, typename... Args>
class IMemento : public Object<IMemento<T, Args...>> {
    friend T;

    /**
     * @brief Function which allows to set the originator of the memento.
     * @param orig Originator to set.
     */
    virtual auto setOriginator(const std::shared_ptr<T>& orig) -> void = 0;

    /**
     * @return The originator of this memento object.
     */
    [[nodiscard]] virtual auto getOriginator() const -> std::shared_ptr<T> = 0;

public:
    /**
     * @brief This function shall enable the memento to restore the state of the originating object.
     * @param args Optional params to be passed to the restore function (defined by the template arguments).
     */
    virtual auto restore(Args... args) -> void = 0;
};

/**
 * @brief Base class for memento objects.
 * @tparam T Class which we shall be able to create an memento for.
 * @tparam Args List of types which shall be passable to the restore function.
 */
template <typename T, typename... Args>
class Memento : public IMemento<T, Args...> {
    friend T;
    std::weak_ptr<T> m_originator;
    auto setOriginator(const std::shared_ptr<T>& orig) -> void override
    {
        if (!orig) {
            return;
        }

        m_originator = orig;
    }
    [[nodiscard]] auto getOriginator() const -> std::shared_ptr<T> override
    {
        return m_originator.lock();
    }

public:
    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<Memento<T, Args...>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<Memento<T, Args...>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<Memento<T, Args...>>;
};

/**
 * @brief Memento originator class. Inherit from this class, if you want this class to
 * create mementos.
 * @tparam T Name of the inheriting class that we shall be able to create an memento from.
 * @tparam Args List of types which shall be passable to the restore function.
 */
template <typename T, typename... Args>
class IOriginator : public Object<T>, public std::enable_shared_from_this<T> {
public:
    /**
     * @brief The implementation shall create an memento for the inheriting class.
     */
    virtual auto createMemento() -> std::shared_ptr<IMemento<T, Args...>> = 0;
};
} // namespace Dg::Patterns
#endif // DG_DESIGNPATTERNS_MEMENTO_H