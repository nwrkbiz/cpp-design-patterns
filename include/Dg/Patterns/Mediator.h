/**
 * @file Mediator.h
 * @brief Mediator pattern template classes.
 *
 * The mediator pattern is a behavioral pattern that can be used to decouple multiple objects via a mediator object.
 * The mediator object forwards ("mediates") the communication between the objects.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html Mediator.drawio.svg
 *
 * ### Code
 *
 * @include Mediator.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_MEDIATOR_H
#define DG_DESIGNPATTERNS_MEDIATOR_H

#include "Dg/Object.h"

namespace Dg::Patterns {

/**
 * @brief (Internal) helper interface, provides a concrete mediator interface for one (concrete) mediatable type.
 * Not intended to be used directly.
 * @tparam Arg Type of an argument that shall be passed to the mediate function.
 * @tparam T Type of the mediatable object (most probably an interface class).
 */
template <typename Arg, typename T>
class ILonelyMediator : public Object<ILonelyMediator<Arg, T>> {
public:
    /**
     * @brief Allows getting one concrete mediatable member object.
     * @param[out] mediatable Output parameter which provides the concrete mediatable object. This works because there is
     * an overload for every concrete object.
     */
    virtual auto getMediatable(std::shared_ptr<T>& mediatable) -> void = 0;

    /**
     * @brief Allows setting the concrete mediatable member objects (usually does not need to be called directly).
     * @param mediatable Concrete mediatable object to set.
     */
    virtual auto setMediatable(const std::shared_ptr<T>& mediatable) -> void = 0;

    /**
     * @brief Mediators mediate function.
     * @param elem Element that triggered the mediation.
     * @param arg Additional argument to be passed to the mediate function.
     */
    virtual auto mediate(const std::shared_ptr<T>& elem, Arg arg) -> void = 0;
};

/**
 * @brief (Internal) helper implementation class, provides a more concrete mediator for one (concrete) mediatable type.
 * Not intended to be used directly.
 * @tparam Arg Type of an argument that shall be passed to the mediate function.
 * @tparam T Type of the mediatable object (most probably an interface class).
 */
template <typename Arg, typename T>
class LonelyMediator : public ILonelyMediator<Arg, T> {
    std::shared_ptr<T> m_mediatable;

public:
    /**
     * @brief Allows getting one concrete mediatable member object.
     * @param[out] mediatable Output parameter which provides the concrete mediatable object. This works because there is
     * an overload for every concrete object.
     */
    auto getMediatable(std::shared_ptr<T>& mediatable) -> void override
    {
        mediatable = m_mediatable;
    }

    /**
     * @brief Allows setting the concrete mediatable member objects (usually does not need to be called directly).
     * @param mediatable Concrete mediatable object to set.
     */
    auto setMediatable(const std::shared_ptr<T>& mediatable) -> void override
    {
        m_mediatable = mediatable;
    }

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<LonelyMediator<Arg, T>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<LonelyMediator<Arg, T>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<LonelyMediator<Arg, T>>;
};

/**
 * @brief Mediator interface. Inherit from this class to implement a mediator class.
 * @tparam Arg Type of an argument that shall be passed to the mediate function.
 * @tparam Ts List of classes that shall be mediatable by this class.
 */
template <typename Arg, typename... Ts>
class IMediator : public LonelyMediator<Arg, Ts>... {
public:
    using ILonelyMediator<Arg, Ts>::getMediatable...;
    using ILonelyMediator<Arg, Ts>::setMediatable...;

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<IMediator<Arg, Ts...>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<IMediator<Arg, Ts...>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<IMediator<Arg, Ts...>>;
};

/**
 * @brief Interface for the mediatable class, can be used e.g. for mocking purposes.
 * @tparam Arg Type of an argument that shall be passed to the mediators mediate function.
 * @tparam T Name of the mediatable object inheriting from this class.
 */
template <typename Arg, typename T>
class IMediatable : public Object<IMediatable<Arg, T>> {
public:
    /**
     * @brief Allows getting the mediator of this mediatable object.
     * @return The mediator of this mediatable object.
     */
    [[nodiscard]] virtual auto getMediator() const -> std::shared_ptr<ILonelyMediator<Arg, T>> = 0;

    /**
     * @brief Allows setting the mediator of this mediatable object.
     *
     * @param mediator Concrete mediator to set.
     */
    virtual auto setMediator(const std::shared_ptr<ILonelyMediator<Arg, T>>& mediator) -> void = 0;
};

/**
 * @brief Inherit your classes which shall be mediatable from this class.
 * @tparam Arg Type of an argument that shall be passed to the mediators mediate function.
 * @tparam T Name of the mediatable object inheriting from this class.
 */
template <typename Arg, typename T>
class Mediatable : public IMediatable<Arg, T>, public std::enable_shared_from_this<T> {
    std::weak_ptr<ILonelyMediator<Arg, T>> m_mediator;

public:
    [[nodiscard]] auto getMediator() const -> std::shared_ptr<ILonelyMediator<Arg, T>> override
    {
        return m_mediator.lock();
    }

    auto setMediator(const std::shared_ptr<ILonelyMediator<Arg, T>>& mediator) -> void override
    {
        if (!mediator) {
            return;
        }

        m_mediator = mediator;
        m_mediator.lock()->setMediatable(this->shared_from_this());
    }

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<T>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<T>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<T>;
};
} // namespace Dg::Patterns
#endif // DG_DESIGNPATTERNS_MEDIATOR_H