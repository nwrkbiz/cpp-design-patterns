/**
 * @file State.h
 * @brief State pattern template classes.
 *
 * The state pattern is a behavioral pattern that can be used to build state machines. This pattern allows encapsulating every
 * state into one class.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html State.drawio.svg
 *
 * ### Code
 *
 * @include State.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_STATE_H
#define DG_DESIGNPATTERNS_STATE_H

#include "Dg/Object.h"

namespace Dg::Patterns {

/**
 * @brief Interface class for the states of the state machine.
 * @tparam C Class of the state machine.
 * @tparam Args List of types to be passed to the states execute function.
 */
template <typename C, typename... Args>
class IState : public Object<IState<C, Args...>> {
public:
    /**
     * @brief Execution function of the state.
     * @param stateMachine Concrete object of the state machine.
     * @param args Optional arguments to be passed to the states execute function.
     */
    virtual auto executeState(const std::shared_ptr<C>& stateMachine, Args... args) -> void = 0;
};

/**
 * @brief Interface class for the state machine.
 *
 * @tparam C Class of the state machine.
 * @tparam Args List of types to be passed to the states machines execute function.
 */
template <typename C, typename... Args>
class IStateContext : public Object<IStateContext<C, Args...>> {
public:
    /**
     * @brief Set the state of the state machine.
     * @param newState New state to set.
     */
    virtual auto setState(const std::shared_ptr<IState<C, Args...>>& newState) -> void = 0;

    /**
     * @return The current state of the state machine.
     */
    [[nodiscard]] virtual auto getState() -> std::shared_ptr<IState<C, Args...>> = 0;

    /**
     * @brief Executes the current state.
     * @param args Optional arguments to be passed to the states execute function.
     */
    virtual auto executeState(Args... args) -> void = 0;
};

/**
 * @brief State machine class.
 * Inherit from this class for a simple state machine.
 * @tparam C Classname of the (inheriting) state machine class.
 * @tparam Args List of types to be passed to the states machines execute function.
 */
template <typename C, typename... Args>
class StateContext : public IStateContext<C, Args...>, public std::enable_shared_from_this<C> {
    std::shared_ptr<IState<C, Args...>> m_state;

public:
    auto setState(const std::shared_ptr<IState<C, Args...>>& newState) -> void override
    {
        m_state = newState;
    }

    [[nodiscard]] auto getState() -> std::shared_ptr<IState<C, Args...>> override
    {
        return m_state;
    }

    auto executeState(Args... args) -> void override
    {
        m_state->executeState(this->shared_from_this(), std::forward<Args>(args)...);
    }

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<C>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<C>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<C>;
};
} // Dg::Patterns
#endif // DG_DESIGNPATTERNS_STATE_H