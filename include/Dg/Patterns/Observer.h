/**
 * @file Observer.h
 * @brief Observer pattern template classes.
 *
 * The observer pattern is a behavioral pattern that allows objects to subscribe to changes of another object.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * To simplify the diagram, the interface class for Observable (IObservable) has been omitted.
 *
 * @image html Observer.drawio.svg
 *
 * ### Code
 *
 * @include Observer.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_OBSERVER_OBSERVABLE_H
#define DG_DESIGNPATTERNS_OBSERVER_OBSERVABLE_H

#include "Dg/Object.h"

#include <algorithm>
#include <vector>

namespace Dg::Patterns {
/**
 * @brief Observer interface class. Inherited classes can observe
 * all classes which inherit from Observable.
 * @tparam I Interface/Base of the observable objects.
 * @tparam Args Optional list of types which shall be passable to the observer via the notify function.
 */
template <typename I, typename... Args>
class IObserver : public Object<IObserver<I, Args...>> {
public:
    /**
     * @brief Called by observable on notify.
     * @param observable Object that triggered the update.
     * @param args Optional arguments passed to the observables notify function.
     */
    virtual auto update(const std::shared_ptr<I>& observable, Args... args) -> void = 0;

protected:
    IObserver() = default;
};

/**
 * @brief Interface for an observable object. (can be used for mocking purposes)
 * @tparam I Interface/Base class which shall be observable.
 * @tparam Args Optional list of types which shall be passable to the observer via the notify function.
 */
template <typename I, typename... Args>
class IObservable : public Object<IObservable<I, Args...>> {
public:
    /**
     * @brief Subscribes an observer.
     * @param obs Observer to subscribe.
     * @return true on success, false otherwise.
     */
    virtual auto subscribe(const std::shared_ptr<IObserver<I, Args...>>& obs) -> bool = 0;

    /**
     * @brief Unsubscribes an observer.
     * @param obs Observer to unsubscribe.
     * @return true on success, false otherwise.
     */
    virtual auto unsubscribe(const std::shared_ptr<IObserver<I, Args...>>& obs) -> bool = 0;

    /**
     * @brief Notifies all subscribed observer objects by calling their update
     * function.
     * @param args Optional arguments to pass to the observer.
     */
    virtual auto notify(Args... args) -> void = 0;

    /**
     * @brief Unsubscribes all subscribed Observers.
     */
    virtual auto unsubscribeAll() -> void = 0;
};

/**
 * @brief Observable class. Inherited classes can notify
 * all classes which inherit from Observer.
 * @tparam I Interface/Base class which shall be observable.
 * @tparam Args Optional list of types which shall be passable to the observer via the notify function.
 */
template <typename I, typename... Args>
class Observable : public IObservable<I, Args...>, public std::enable_shared_from_this<I> { // NOLINT
    std::vector<std::shared_ptr<IObserver<I, Args...>>> m_observers;

public:
    auto subscribe(const std::shared_ptr<IObserver<I, Args...>>& obs) -> bool override
    {
        if (!obs) {
            return false;
        }

        m_observers.push_back(obs);
        return true;
    }

    auto unsubscribe(const std::shared_ptr<IObserver<I, Args...>>& obs) -> bool override
    {
        const auto found = std::find(m_observers.begin(), m_observers.end(), obs);
        if (found == m_observers.end()) {
            return false;
        }

        m_observers.erase(found);
        return true;
    }

    auto unsubscribeAll() -> void override
    {
        m_observers.clear();
    }

    auto notify(Args... args) -> void override
    {
        for (const auto& curObs : m_observers) {
            curObs->update(this->shared_from_this(), std::forward<Args>(args)...);
        }
    }

    /**
     * @brief Destructor
     */
    ~Observable() override = default;

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<I>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<I>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<I>;

protected:
    /**
     * @brief Constructor
     */
    Observable() = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Observable(const Observable& origin) = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Observable(Observable&& origin) noexcept = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(const Observable& origin) -> Observable& = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(Observable&& origin) noexcept -> Observable& = default;
};
} // namespace Dg::Patterns
#endif // DG_DESIGNPATTERNS_OBSERVER_OBSERVABLE_H