/**
 * @file Property.h
 * @brief Property idiom template classes.
 *
 * The property idiom can be used to create a member that provides a flexible mechanism to read, write, or compute
 * the value of a private field.
 *
 * Example
 * -------
 *
 * ### Code
 *
 * @include Property.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_PROPERTY_H
#define DG_DESIGNPATTERNS_PROPERTY_H

#include "Dg/Object.h"

#include <functional>
#include <string>

/**
 * @brief Namespace containing the property idiom implementation.
 */
namespace Dg::Patterns::Property {

/**
 * @brief Interface class for read-only properties.
 * @tparam T datatype of the property
 */
template <typename T>
class IReadOnly : public Object<IReadOnly<T>> {
public:
    /**
     * @brief Access operator
     * @return The value.
     */
    virtual operator T() const = 0;

    /**
     * @brief Pointer Operator
     * @return Reference to the value.
     */
    virtual auto operator->() -> T = 0;
};

/**
 * @brief Interface class for properties.
 * @tparam T datatype of the property
 */
template <typename T>
class IProperty : public IReadOnly<T> {
public:
    /**
     * @brief Assignment Operator
     * @param value Value to assign
     * @return Reference to the value.
     */
    virtual auto operator=(T value) -> T& = 0; // NOLINT
};

/**
 * @brief Implementation for auto properties. This class manages the property member on its own.
 * Because of that it is easier but less flexible to use.
 * @tparam T datatype of the property
 * @tparam F optional friend class with access to m_val for ReadOnly properties (usually the class holding the properties).
 */
template <typename T, typename F = void>
class Auto : public IProperty<T> {
public:
    /**
     * @brief Convenience definition for a property getter function.
     */
    using Getter = std::function<const T(T const&)>;

    /**
     * @brief Convenience definition for a property setter function.
     */
    using Setter = std::function<void(T&, T const&)>;

    /**
     * @brief Constructor
     * @param defaultVal Default value to set
     * @param get Getter function
     * @param set Setter function
     */
    Auto(
        T defaultVal = {},
        Getter get = [](T const& propertyVal) -> T { return propertyVal; },
        Setter set = [](T& propertyVal, T const& assignVal) -> void { propertyVal = assignVal; })
        : m_val(std::move(defaultVal))
        , m_get(std::move(get))
        , m_set(std::move(set)) {};

    operator T() const override
    {
        return this->m_get(m_val);
    };

    auto operator->() -> T override
    {
        return this->m_get(m_val);
    }

    auto operator=(T value) -> T& override // NOLINT
    {
        this->m_set(m_val, value);
        return m_val;
    }

    /**
     * @brief Implementation for read-only auto properties. This class manages the property member on its own.
     * Because of that it is easier but less flexible to use.
     * @tparam T datatype of the property
     */
    class ReadOnly : public IReadOnly<T> {
        friend F;

    public:
        /**
         * @brief Constructor
         * @param defaultVal Default value to set
         * @param get Getter function
         */
        ReadOnly(
            T defaultVal = {},
            Getter get = [](T const& propertyVal) -> T { return propertyVal; })
            : m_val(std::move(defaultVal))
            , m_get(std::move(get)) {};

        operator T() const override
        {
            return this->m_get(m_val);
        };

        auto operator->() -> T override
        {
            return this->m_get(m_val);
        }

    private:
        T m_val;
        Getter m_get;
    };

private:
    T m_val;
    Getter m_get;
    Setter m_set;
};

/**
 * @brief Implementation for manual properties. If you use the lambda capture, you can use
 * these kind of properties to manage your own private member variables.
 * @tparam T datatype of the property
 */
template <typename T>
class Manual : public IProperty<T> {
public:
    /**
     * @brief Convenience definition for a property getter function.
     */
    using Getter = std::function<T&()>;

    /**
     * @brief Convenience definition for a property setter function.
     */
    using Setter = std::function<void(T const&)>;

    /**
     * @brief Constructor
     * @param get Getter function
     * @param set Setter function
     */
    Manual(
        Getter get,
        Setter set)
        : m_get(std::move(get))
        , m_set(std::move(set)) {};

    operator T() const override
    {
        return this->m_get();
    };

    auto operator->() -> T override
    {
        return this->m_get();
    }

    auto operator=(T value) -> T& override // NOLINT
    {
        this->m_set(value);
        return this->m_get();
    }

    /**
     * @brief Implementation for readonly manual properties. If you use the lambda capture, you can use
     * these kind of properties to manage your own private member variables.
     * @tparam T datatype of the property
     */
    class ReadOnly : public IReadOnly<T> {
    public:
        /**
         * @brief Constructor
         * @param get Getter function
         */
        ReadOnly(
            Getter get)
            : m_get(std::move(get)) {};

        operator T() const override
        {
            return this->m_get();
        };

        auto operator->() -> T override
        {
            return this->m_get();
        }

    private:
        Getter m_get;
    };

private:
    Getter m_get;
    Setter m_set;
};

/**
 * @brief Overload for the << operator to simplify the streaming of std::strings.
 */
inline auto operator<<(std::ostream& outStream, IReadOnly<std::string> const& stringProp) -> std::ostream&
{
    return outStream << std::string(stringProp);
}

}

#endif // DG_DESIGNPATTERNS_PROPERTY_H