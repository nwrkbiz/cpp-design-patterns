/**
 * @file Prototype.h
 * @brief Prototype pattern template classes.
 *
 * The prototype pattern is a creational design pattern that allows creating copies from abstract (interface) objects.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html Prototype.drawio.svg
 *
 * ### Code
 *
 * @include Prototype.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_PROTOTYPE_H
#define DG_DESIGNPATTERNS_PROTOTYPE_H

#include "Dg/Object.h"

namespace Dg::Patterns {

/**
 * @brief Interface class for the Prototype class. Can be used e.g. for mocking purposes.
 * @tparam I Interface/Base class of the prototype.
 * @tparam Args List of optional types, which can be passed to the clone function.
 */
template <typename I, typename... Args>
class IPrototype : public I {
public:
    /**
     * @brief Clones the concrete object and returns a shared pointer.
     * @param args Optional arguments to be passed to the clone function.
     * @return Shared pointer to the cloned object.
     */
    virtual auto cloneShared(Args... args) -> std::shared_ptr<IPrototype<I, Args...>> = 0;

    /**
     * @brief Clones the concrete object and returns a unique pointer.
     * @param args Optional arguments to be passed to the clone function.
     * @return Unique pointer to the cloned object.
     */
    virtual auto cloneUnique(Args... args) -> std::unique_ptr<IPrototype<I, Args...>> = 0;

    /**
     * @brief Clones the concrete object and returns a raw pointer, yields ownership.
     * @param args Optional arguments to be passed to the clone function.
     * @return Raw pointer to the cloned object.
     */
    virtual auto cloneRaw(Args... args) -> IPrototype<I, Args...>* = 0;

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<IPrototype<I, Args...>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<IPrototype<I, Args...>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<IPrototype<I, Args...>>;
};

/**
 * @brief Implementation of the Prototype template class.
 * Inherit from this class and implement a proper copy constructor
 * to make your objects cloneable. You may also override the clone
 * functions.
 * @tparam I Interface/Base class of the prototype.
 * @tparam T Concrete prototype class.
 * @tparam Args List of optional types, which can be passed to the clone function.
 */
template <typename I, typename T, typename... Args>
class Prototype : public IPrototype<I, Args...> {
protected:
    /**
     * @brief Constructor
     */
    Prototype() = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Prototype(const Prototype& origin) = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Prototype(Prototype&& origin) noexcept = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(const Prototype& origin) -> Prototype& = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(Prototype&& origin) noexcept -> Prototype& = default;

public:
    /**
     * @brief Destructor
     */
    ~Prototype() override = default;

    auto cloneShared(Args... /*unused*/) -> std::shared_ptr<IPrototype<I, Args...>> override
    {
        return std::make_shared<T>(*static_cast<T*>(this));
    }

    auto cloneUnique(Args... /*unused*/) -> std::unique_ptr<IPrototype<I, Args...>> override
    {
        return std::make_unique<T>(*static_cast<T*>(this));
    }

    auto cloneRaw(Args... /*unused*/) -> IPrototype<I, Args...>* override
    {
        return new T(*static_cast<T*>(this)); // NOLINT
    }

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<Prototype<I, T, Args...>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<Prototype<I, T, Args...>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<Prototype<I, T, Args...>>;
};
} // Dg::Patterns
#endif // DG_DESIGNPATTERNS_PROTOTYPE_H