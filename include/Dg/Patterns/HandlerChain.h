/**
 * @file HandlerChain.h
 * @brief Chain of responsibility pattern template classes.
 *
 * The chain of responsibility pattern is a behavioral pattern that can be used to pass a handler or data structure along a
 * chain of handler objects. This can be used for instance to build up image manipulation pipelines.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html HandlerChain.drawio.svg
 *
 * ### Code
 *
 * @include HandlerChain.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_HANDLERCHAIN_H
#define DG_DESIGNPATTERNS_HANDLERCHAIN_H

#include "Dg/Object.h"

#include <vector>

namespace Dg::Patterns {

/**
 * @brief Interface class for the Handler class. Can be used e.g. for mocking purposes.
 * @tparam I Interface/Base class of the handler.
 * @tparam Args Type(s) that shall be handled by the chain.
 */
template <typename I, typename... Args>
class IHandler : public Object<IHandler<I, Args...>> {
public:
    /**
     * @brief Allows setting the next handler.
     */
    virtual auto setNext(const std::shared_ptr<I>& next) -> void = 0;

    /**
     * @brief Allows getting the currently set next handler.
     * @return Next handler that is set.
     */
    [[nodiscard]] virtual auto getNext() const -> std::shared_ptr<I> = 0;

    /**
     * @brief Handle function each handler needs to implement.
     * @param toHandle (List of) Object(s) that the handler shall work with.
     * @return The implementation shall return true if the next handler shall be invoked, false otherwise.
     */
    virtual auto handle(Args... toHandle) -> bool = 0;

    /**
     * @brief Starts invoking the handler chain.
     * @param toHandle (List of) Object(s) that the handlers shall work with.
     */
    virtual auto startHandling(Args... toHandle) -> void = 0;

    /**
     * @brief Recursive convenience function allowing to create a whole handler chain with one function call.
     * @param first First handler element.
     * @param second Second handler element.
     * @param params List of additional handler elements.
     * @return First element of the handler chain.
     */
    template <typename... recursionParams>
    static auto createHandlerChain(const std::shared_ptr<I>& first, const std::shared_ptr<I>& second, recursionParams... params) -> std::shared_ptr<I>
    {
        first->setNext(second);
        createHandlerChain(second, std::forward<recursionParams>(params)...);
        return first;
    }

    /**
     * @brief Recursion bottom for handler chain creation.
     * @param first First handler element.
     * @param second Second handler element.
     * @return First element of the handler chain.
     */
    static auto createHandlerChain(const std::shared_ptr<I>& first, const std::shared_ptr<I>& second) -> std::shared_ptr<I>
    {
        first->setNext(second);
        return first;
    }
};

/**
 * @brief Implementation of the Handler template class.
 * @tparam I Interface/Base class of the handler.
 * @tparam Args Type(s) that shall be handled by the chain.
 */
template <typename I, typename... Args>
class Handler : public IHandler<I, Args...> {
    std::shared_ptr<I> m_next = {};

public:
    auto setNext(const std::shared_ptr<I>& next) -> void override
    {
        m_next = next;
    }
    [[nodiscard]] auto getNext() const -> std::shared_ptr<I> override
    {
        return m_next;
    }
    auto startHandling(Args... toHandle) -> void override // NOLINT(misc-no-recursion)
    {
        if (!this->handle(std::forward<Args>(toHandle)...) || !m_next) {
            return;
        }

        m_next->startHandling(std::forward<Args>(toHandle)...);
    };

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<I>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<I>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<I>;
};
} // Dg::Patterns
#endif // DG_DESIGNPATTERNS_HANDLERCHAIN_H