/**
 * @file PassKey.h
 * @brief Passkey idiom class.
 *
 * The passkey idiom is a behavioral idiom that allows locking functions via a key object.
 * This can be used for instance to lock pure virtual function implementations to be only called from base class.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html PassKey.drawio.svg
 *
 * ### Code
 *
 * @include PassKey.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_PASSKEY_H
#define DG_DESIGNPATTERNS_PASSKEY_H

#include "Dg/Object.h"

namespace Dg::Patterns {
/**
 * @brief Key class implementing passkey idiom.
 * Can be used for instance to lock pure virtual function implementations so that they
 * can only be called from the base class.
 */
template <typename T>
class Key : public Object<Key<T>> { // NOLINT
    friend T;
    Key() = default;

public:
    /**
     * @brief Destructor
     */
    ~Key() override = default;

    Key(const Key&) = delete;
    Key(Key&&) noexcept = delete;
    auto operator=(const Key&) -> Key& = delete;
    auto operator=(Key&&) noexcept -> Key& = delete;
};
} // namespace Dg::Patterns
#endif // DG_DESIGNPATTERNS_PASSKEY_H