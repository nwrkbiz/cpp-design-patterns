/**
 * @file Composite.h
 * @brief Composite pattern template classes.
 *
 * The composite pattern is a structural design pattern that can be used to build tree structures.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * To simplify the diagram, the interface class for Composite (IComposite) has been omitted.
 *
 * @image html Composite.drawio.svg
 *
 * ### Code
 *
 * @include Composite.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_COMPOSITE_H
#define DG_DESIGNPATTERNS_COMPOSITE_H

#include "Dg/Object.h"

#include <algorithm>
#include <iterator>
#include <vector>

/**
 * @brief Namespace containing all design pattern template classes.
 */
namespace Dg::Patterns {

/**
 * @brief Namespace containing iterator classes for the composite pattern.
 */
namespace Iterators::Composite {

    /**
     * @brief Enum that provides hints to the iterator classes on how the iterator creation was invoked.
     */
    enum class CreationType {
        /**
         * @brief Unknown creation type.
         */
        unknown = 0,

        /**
         * @brief Created via the containers begin() function.
         */
        begin,

        /**
         * @brief Created via the containers end() function.
         */
        end
    };

    /**
     * @brief Iterator that allows navigating through the composite elements in pre-order (left to right)
     * @tparam T Interface/Base class of one tree node.
     */
    template <typename T>
    class PreOrderIterator : public Object<PreOrderIterator<T>> {
        std::shared_ptr<T> m_cur { nullptr };

    public:
        /**
         * @brief Constructor
         * @param root Node to start iteration with.
         * @param createdVia Enum that hints the iterator on how the construction was invoked (defaults to CreationType::unknown).
         */
        PreOrderIterator(const std::shared_ptr<T>& root, CreationType createdVia = CreationType::unknown)
        {
            if (root && (createdVia == CreationType::unknown || createdVia == CreationType::begin)) {
                m_cur = root;
            }
        }

        /**
         * @brief Dereference Operator
         * @return The current element.
         */
        [[nodiscard]] auto operator*() -> std::shared_ptr<T>&
        {
            return m_cur;
        }

        /**
         * @brief Increment Operator
         * @return An iterator pointing to the next element.
         */
        auto operator++() -> PreOrderIterator<T>&
        {
            if (m_cur->getChildren().empty()) {
                std::shared_ptr<T> parent { m_cur };
                std::shared_ptr<T> comp { m_cur };
                m_cur = nullptr;
                while ((parent = parent->getParent()) != nullptr) {
                    auto nextElemIt = std::find(parent->getChildren().begin(), parent->getChildren().end(), comp);
                    if ((nextElemIt + 1) != parent->getChildren().end()) {
                        m_cur = *(nextElemIt + 1);
                        break;
                    }
                    comp = parent;
                }
            } else {
                m_cur = *(m_cur->getChildren().begin());
            }
            return *this;
        }

        /**
         * @brief Inequality operator.
         * @return false if the iteration end has been reached, true otherwise.
         */
        [[nodiscard]] auto operator!=(const PreOrderIterator<T>& /* other */) -> bool
        {
            return !(m_cur == nullptr);
        }
    };

    /**
     * @brief Iterator that allows navigating through the composite elements in post-order (right to left)
     * @tparam T Interface/Base class of one tree node.
     */
    template <typename T>
    class PostOrderIterator : public Object<PostOrderIterator<T>> {
        std::shared_ptr<T> m_cur { nullptr };

    public:
        /**
         * @brief Constructor
         * @param root Node to start iteration with.
         * @param createdVia Enum that hints the iterator on how the construction was invoked (defaults to CreationType::unknown).
         */
        PostOrderIterator(const std::shared_ptr<T>& root, CreationType createdVia = CreationType::unknown)
        {
            if (root && (createdVia == CreationType::unknown || createdVia == CreationType::begin)) {
                m_cur = root;
            }
        }

        /**
         * @brief Dereference Operator
         * @return The current element.
         */
        [[nodiscard]] auto operator*() -> std::shared_ptr<T>&
        {
            return m_cur;
        }

        /**
         * @brief Increment Operator
         * @return An iterator pointing to the next element.
         */
        auto operator++() -> PostOrderIterator<T>&
        {
            if (m_cur->getChildren().empty()) {
                std::shared_ptr<T> parent { m_cur };
                std::shared_ptr<T> comp { m_cur };
                m_cur = nullptr;
                while ((parent = parent->getParent()) != nullptr) {
                    auto nextElemIt = std::find(parent->getChildren().rbegin(), parent->getChildren().rend(), comp);
                    if ((nextElemIt + 1) != parent->getChildren().rend()) {
                        m_cur = *(nextElemIt + 1);
                        break;
                    }
                    comp = parent;
                }
            } else {
                m_cur = *(m_cur->getChildren().end() - 1);
            }
            return *this;
        }

        /**
         * @brief Inequality operator.
         * @return false if the iteration end has been reached, true otherwise.
         */
        [[nodiscard]] auto operator!=(const PostOrderIterator<T>& /* other */) -> bool
        {
            return !(m_cur == nullptr);
        }
    };
}

/**
 * @brief Interface class for the Composite class. Can be used e.g. for mocking purposes.
 * @tparam T Interface/Base class of one tree node.
 * @tparam I Iterator to be returned by begin() and end() (Defaults to a simple pre-order iterator).
 */
template <typename T, typename I = Iterators::Composite::PreOrderIterator<T>>
class IComposite : public Object<T> {
public:
    /**
     * @brief Allows getting the children of this node.
     * @return A vector containing all children.
     */
    [[nodiscard]] virtual auto getChildren() const -> const std::vector<std::shared_ptr<T>>& = 0;

    /**
     * @brief Allows getting the parent node, if existent.
     * @return The parent node, nullptr if there is no parent node.
     */
    [[nodiscard]] virtual auto getParent() const -> std::shared_ptr<T> = 0;

    /**
     * @brief Allows setting the parent node.
     * @param parent Parent node to set.
     */
    virtual auto setParent(const std::shared_ptr<T>& parent) -> void = 0;

    /**
     * @brief Helper function that allows adding a new child node.
     * @param child Child node to add.
     */
    virtual auto addChild(const std::shared_ptr<T>& child) -> void = 0;

    /**
     * @brief Helper function that allows removing a formerly added child node.
     * @param child Node to remove.
     */
    virtual auto removeChild(const std::shared_ptr<T>& child) -> void = 0;

    /**
     * @brief Helper function that can be used to determine if this node is a leaf node (has no children).
     * @return true if this is a leaf node, false otherwise.
     */
    [[nodiscard]] virtual auto isLeaf() -> bool = 0;

    /**
     * @brief Allows getting a iterator which points to the beginning (the element beginning represents depends on the iterator).
     * @return Iterator pointing to the beginning of the composite structure.
     */
    [[nodiscard]] virtual auto begin() -> I = 0;

    /**
     * @brief Allows getting a iterator which points to the end (the element end represents depends on the iterator).
     * @return Iterator pointing to the end of the composite structure.
     */
    [[nodiscard]] virtual auto end() -> I = 0;
};

/**
 * @brief (Internal) helper class, implements all tree "parent" functions. Not intended to
 * be used directly.
 * @tparam T Interface/Base class of one tree node.
 * @tparam I Iterator to be returned by begin() and end() (Defaults to a simple pre-order iterator).
 */
template <typename T, typename I = Iterators::Composite::PreOrderIterator<T>>
class ParentOnly : public IComposite<T, I> {
    std::weak_ptr<T> m_parent;

public:
    /**
     * @brief Allows getting the parent node, if existent.
     * @return The parent node, nullptr if there is no parent node.
     */
    [[nodiscard]] auto getParent() const -> std::shared_ptr<T> override
    {
        return m_parent.lock();
    }

    /**
     * @brief Allows setting the parent node.
     * @param parent Parent node to set.
     */
    auto setParent(const std::shared_ptr<T>& parent) -> void override
    {
        m_parent = parent;
    }
};

/**
 * @brief Implementation of Composite class. Inherit your class, if you want to be
 * able to store objects of your class in a tree like structure.
 * @tparam T Interface/Base class of one tree node.
 * @tparam I Iterator to be returned by begin() and end() (Defaults to a simple pre-order iterator).
 */
template <typename T, typename I = Iterators::Composite::PreOrderIterator<T>>
class Composite : public ParentOnly<T, I>, public std::enable_shared_from_this<T> { // NOLINT, clang-tidy bug
    std::vector<std::shared_ptr<T>> m_children;

protected:
    /**
     * @brief Constructor
     */
    Composite() = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Composite(const Composite& origin) = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Composite(Composite&& origin) noexcept = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(const Composite& origin) -> Composite& = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(Composite&& origin) noexcept -> Composite& = default;

public:
    /**
     * @brief Destructor
     */
    ~Composite() override = default;

    auto addChild(const std::shared_ptr<T>& child) -> void override
    {
        child->setParent(this->shared_from_this());
        m_children.push_back(child);
    }

    auto removeChild(const std::shared_ptr<T>& child) -> void override
    {
        const auto found = std::find(m_children.begin(), m_children.end(), child);
        if (found == m_children.end()) {
            return;
        }

        child->setParent(nullptr);
        m_children.erase(found);
    }

    [[nodiscard]] auto getChildren() const -> const std::vector<std::shared_ptr<T>>& override
    {
        return m_children;
    }

    [[nodiscard]] auto isLeaf() -> bool override
    {
        return m_children.empty();
    }

    [[nodiscard]] auto begin() -> I override
    {
        return I(this->shared_from_this(), Iterators::Composite::CreationType::begin);
    }

    [[nodiscard]] auto end() -> I override
    {
        return I(this->shared_from_this(), Iterators::Composite::CreationType::end);
    }
};

} // namespace Dg::Patterns
#endif // DG_DESIGNPATTERNS_COMPOSITE_H