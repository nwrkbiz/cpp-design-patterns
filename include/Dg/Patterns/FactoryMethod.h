/**
 * @file FactoryMethod.h
 * @brief FactoryMethod pattern template classes.
 *
 * The factory method pattern is a creational design pattern that is used to abstract away the creation of concrete objects so that
 * the user only depends on the objects interface. Hence this is usually used to increase loose coupling of objects.
 *
 * Example
 * -------
 *
 * ### UML
 *
 * @image html FactoryMethod.drawio.svg
 *
 * ### Code
 *
 * @include FactoryMethod.cpp
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_DESIGNPATTERNS_FACTORYMETHOD_H
#define DG_DESIGNPATTERNS_FACTORYMETHOD_H

#include "Dg/Object.h"

namespace Dg::Patterns {

/**
 * @brief Interface class for the FactoryMethod class. Can be used e.g. for mocking purposes.
 * @tparam I Interface/Base class of the concrete type.
 * @tparam Args List of argument types which may be passed to the concrete elements constructor.
 */
template <typename I, typename... Args>
class IFactoryMethod : public Object<IFactoryMethod<I, Args...>> {
public:
    /**
     * @brief Constructs a shared pointer of a concrete object.
     * @param args Arguments which are needed to construct the object.
     * @return Shared pointer of the concrete object.
     */
    virtual auto createShared(Args... args) -> std::shared_ptr<I> = 0;

    /**
     * @brief Constructs a unique pointer of a concrete object.
     * @param args Arguments which are needed to construct the object.
     * @return Unique pointer of the concrete object.
     */
    virtual auto createUnique(Args... args) -> std::unique_ptr<I> = 0;

    /**
     * @brief Constructs a raw pointer of a concrete object, yields ownership.
     * @param args Arguments which are needed to construct the object.
     * @return Raw pointer of the concrete object.
     */
    [[nodiscard]] virtual auto createRaw(Args... args) -> I* = 0;
};

/**
 * @brief Implementation of the FactoryMethod template class.
 * @tparam I Interface/Base class of the concrete type.
 * @tparam T Concrete class of which objects shall be created.
 * @tparam Args List of argument types which may be passed to the concrete elements constructor.
 */
template <typename I, typename T, typename... Args>
class FactoryMethod : public IFactoryMethod<I, Args...> {
public:
    auto createShared(Args... args) -> std::shared_ptr<I> override
    {
        return std::make_shared<T>(std::forward<Args>(args)...);
    };

    auto createUnique(Args... args) -> std::unique_ptr<I> override
    {
        return std::make_unique<T>(std::forward<Args>(args)...);
    };

    [[nodiscard]] auto createRaw(Args... args) -> I* override
    {
        return new T(std::forward<Args>(args)...); // NOLINT
    };

    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<FactoryMethod<I, T, Args...>>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<FactoryMethod<I, T, Args...>>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<FactoryMethod<I, T, Args...>>;
};
} // Dg::Patterns
#endif // DG_DESIGNPATTERNS_FACTORYMETHOD_H