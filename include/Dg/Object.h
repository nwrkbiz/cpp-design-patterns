/**
 * @file Object.h
 * @brief Base class of all classes.
 * @author Daniel Giritzer, MSc
 * @copyright Copyright 2023-2024 Daniel Giritzer, MSc
 * @license The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef DG_BASECLASS_OBJECT_H
#define DG_BASECLASS_OBJECT_H
#include <memory>

/**
 * @brief Namespace Dg (Danube goodies)
 */
namespace Dg {

/**
 *  @brief Interface for all classes.
 */
class IObject {
public:
    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<IObject>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<IObject>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<IObject>;

    /**
     * @brief Destructor
     */
    virtual ~IObject() = default;

protected:
    /**
     * @brief Constructor
     */
    IObject() = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    IObject(const IObject& origin) = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    IObject(IObject&& origin) noexcept = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(const IObject& origin) -> IObject& = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(IObject&& origin) noexcept -> IObject& = default;
};

/**
 *  @brief Base class of all classes.
 *
 *  @tparam Class name of inheriting class.
 */
template <typename T>
class Object : public IObject {
public:
    /**
     * @brief Convenience definition for a shared pointer.
     */
    using SPtr = std::shared_ptr<T>;

    /**
     * @brief Convenience definition for a unique pointer.
     */
    using UPtr = std::unique_ptr<T>;

    /**
     * @brief Convenience definition for a weak pointer.
     */
    using WPtr = std::weak_ptr<T>;
    ~Object() override = default;

protected:
    /**
     * @brief Constructor
     */
    Object() = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Object(const Object& origin) = default;

    /**
     * @brief Copy-Constructor
     * @param origin Object to copy
     */
    Object(Object&& origin) noexcept = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(const Object& origin) -> Object& = default;

    /**
     * @brief Assignment Operator
     * @param origin Object to assign
     * @return Assignable copy.
     */
    auto operator=(Object&& origin) noexcept -> Object& = default;
};
} // namespace Dg
#endif // DG_BASECLASS_OBJECT_H